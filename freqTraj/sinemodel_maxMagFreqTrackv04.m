addpath('sinemodel');

%%%%%%%%%%%%%%%%
% pick out peak tracks
% look through all tracks at each time slice
% pick frequency of track that has the largest magnitude
% only pick frequency tracks that do not include frequencies
%   outside of the saxophone frequency range
% then combine the frequency tracks that have all frequencies
%   in the saxophone range (how to combine?)
% 
% working OK -- just one doesn't work the best i think gesture4.wav
%   because it goes back up
% ideas for improvement:
%   wait it's not my problem, the problem is in the
%       found frequency track from Dan Ellis's code
%   but let's see...is it noisy there? maybe we can choose to not
%       put out anything if it is noisy there...kind of like attacks
%       some weirdly noisy release?
%%%%%%%%%%%%%%%%


% set window size (do tests for different window sizes!)
winSize = 1024;

saxMinFreq = 125;
saxMaxFreq = 900;

% [x, fs] = wavread('gestures/gesture0.wav');
% [x, fs] = wavread('gestures/gesture1.wav');
% [x, fs] = wavread('gestures/gesture2.wav');
% [x, fs] = wavread('gestures/gesture3.wav');
 [x, fs] = wavread('gestures/gesture4.wav');
% [x, fs] = wavread('gestures/gesture5.wav');
% [x, fs] = wavread('gestures/gesture6.wav');
% [x, fs] = wavread('gestures/gesture7.wav');

% analysis

S = specgram(x, winSize);
[R,M] = extractrax(abs(S), 0.1);
tt = [1:size(R,2)] * (winSize/2/fs);

F = R*fs/winSize;
specgram(x, winSize, fs);
colormap(1-gray)
hold on
plot(tt, F', 'r');

% try to get a frequency trajectory by choosing the 'best' frequency track
nTimeSlices = length(tt);
freqTraj = zeros(1,nTimeSlices);
binTraj = zeros(1,nTimeSlices);
magTraj = zeros(1, nTimeSlices);

nTracks = size(F,1);
% for each frequency track
% count occurences of frequencies outside
nFreqsOutside = zeros(nTracks,1);
for track=1:nTracks
    nFreqsOutside(track) = sum(F(track,:) < saxMinFreq) + ...
        sum(F(track,:) > saxMaxFreq);
end


possibleTracks = find(nFreqsOutside == min(nFreqsOutside(:)));

% the bottom code shows that the magnitudes don't tell us anything 
% about F0 or the frequency track to
% choose:
% for track=1:length(possibleTracks)
%     mags = M(track,:);
%     mags(isnan(mags)) = 0;
%     mean(mags);
% end

possibleFT = F(possibleTracks,:);

% currFreqTrack will index the correct frequency track in possibleTracks
% so to get the correct frequency track index in F, we have to do
% possibleTracks(currFreqTrack)
currFreqTrack = 0;
freqTraj = zeros(nTimeSlices,1);
magTraj = zeros(nTimeSlices,1);
chosenTracks = zeros(nTimeSlices,1);
% after the first chosen track is done, find the next best one...
for ts=1:nTimeSlices
    % check if any track is not nan
    if sum(~isnan(possibleFT(:,ts))) > 0
        if currFreqTrack ~= 0
            % we are already following a track
            if isnan(possibleFT(currFreqTrack,ts))
                % the current one is nan so we should switch to the next
                [~, mi] = min(possibleFT(:,ts));
                currFreqTrack = mi;
                freqTraj(ts) = F(possibleTracks(currFreqTrack),ts);
                magTraj(ts) = M(possibleTracks(currFreqTrack),ts);
            else
                % the current one is not nan, keep following it
                freqTraj(ts) = F(possibleTracks(currFreqTrack),ts);
                magTraj(ts) = M(possibleTracks(currFreqTrack),ts);
            end
        else
            % we are currently not following a track 
            % choose a new track (the one with lowest freq)
            [~, mi] = min(possibleFT(:,ts));
            currFreqTrack = mi;
            freqTraj(ts) = F(possibleTracks(currFreqTrack),ts);
            magTraj(ts) = M(possibleTracks(currFreqTrack),ts);
        end
    else
        currFreqTrack = 0;
        continue;
    end
    chosenTracks(ts) = currFreqTrack;   % save the order of tracks chosen
end


% plot 
figure;
plot(tt, freqTraj, 'linewidth', 2);
xlabel('time (FFT frames)');
ylabel('frequency (Hz)');
title(sprintf('best frequency track estimate (from combined method)'));
grid on

figure;
plot(chosenTracks, 'linewidth', 2);
xlabel('time (FFT frames)');
ylabel('chosen frequency track index');
title(sprintf('chosen frequency tracks over time, indexed by possible frequency tracks'));
grid on


% resynthesize with sine waves
xr1 = synthtrax(freqTraj',magTraj',fs,winSize,winSize/2);
soundsc(x,fs);
pause(length(x)/fs + 0.1);
soundsc(xr1, fs);


