addpath('sinemodel');

%%%%%%%%%%%%%%%%
% method for extracting fundamental frequency trajectory from sax gesture:
% 1. pick out frequency tracks from peaks (using Dan Ellis's code)
% 2. narrow down possible frequency tracks to frequency tracks that only
%   contain frequencies in the saxophone range
% 3. for each time frame, we need to choose the best frequency track that
%   we can be on -- (this is the frequency track with the lowest frequency)
%   for each time frame:
%       if some frequency tracks exist:
%           if we are currently following a track:
%               if the current track has died or there exists another track
%                   with a lower frequency, follow another track with the
%                   lowest frequency
%               otherwise, keep following the current track
%           else if we are not following a track:
%               choose the frequency track with the lowest frequency
%       else if no frequency tracks currently exist:
%           continue
% 4. look at resulting magnitude trajectory and for any magnitude value
%   below -30dB, set the corresponding frequency trajectory value to 0
% NOTE: THIS ONE NOW USES THE PHASE PRESERVING ANALYSIS TO GET A BETTER
% MAGNITUDE READING!!
%%%%%%%%%%%%%%%%

% set window size
winSize = 1024;

saxMinFreq = 125;
saxMaxFreq = 900;

inputDir = 'gestures/';
inputName = 'gesture7';

freqOutDir = 'freqTrajOutput/';
magOutDir = 'magTrajOutput/';

[x, fs] = wavread([inputDir inputName '.wav']);

%[x, fs] = wavread('/Users/jenniferhsu/Documents/ucsd/saxophoneProject/data/highCWithLowC14.wav');

%% phase-preserving analysis: better analysis/synthesis from Dan Ellis's tutorial page

% spectrogram and instantaneous frequency gram
[I,S]=ifgram(x,winSize,winSize,winSize/2,fs);
% extract the peak tracks based on the new STFT
[R,M]=extractrax(abs(S), 0.1);
% interpolated IF-gram values for exact track frequencies (Hz)
F = colinterpvals(R,I);

% plot spectrogram and frequency tracks
figure;
tt = [1:size(R,2)] * (winSize/2/fs);
specgram(x, winSize, fs);
colormap(1-gray)
hold on
plot(tt, F', 'r');


%% try to get a frequency trajectory by choosing the 'best' frequency track

nTimeSlices = length(tt);
nTracks = size(F,1);
% for each frequency track
% count occurences of frequencies outside
nFreqsOutside = zeros(nTracks,1);
for track=1:nTracks
    nFreqsOutside(track) = sum(F(track,:) < saxMinFreq) + ...
        sum(F(track,:) > saxMaxFreq);
end

possibleTracks = find(nFreqsOutside == min(nFreqsOutside(:)));
possibleFT = F(possibleTracks,:);

% currFreqTrack will index the correct frequency track in possibleTracks
% so to get the correct frequency track index in F, we have to do
% possibleTracks(currFreqTrack)
currFreqTrack = 0;
freqTraj = zeros(nTimeSlices,1);
magTraj = zeros(nTimeSlices,1);
chosenTracks = zeros(nTimeSlices,1);
% after the first chosen track is done, find the next best one...
for ts=1:nTimeSlices
    % check if any track is not nan
    if sum(~isnan(possibleFT(:,ts))) > 0
        if currFreqTrack ~= 0
            % we are already following a track
            if isnan(possibleFT(currFreqTrack,ts))
                % the current one is nan so we should switch to the next
                [~, mi] = min(possibleFT(:,ts));
                currFreqTrack = mi;
                freqTraj(ts) = F(possibleTracks(currFreqTrack),ts);
                magTraj(ts) = M(possibleTracks(currFreqTrack),ts);
            else
                % the current one is not nan, keep following it
                freqTraj(ts) = F(possibleTracks(currFreqTrack),ts);
                magTraj(ts) = M(possibleTracks(currFreqTrack),ts);
            end
        else
            % we are currently not following a track 
            % choose a new track (the one with lowest freq)
            [~, mi] = min(possibleFT(:,ts));
            currFreqTrack = mi;
            freqTraj(ts) = F(possibleTracks(currFreqTrack),ts);
            magTraj(ts) = M(possibleTracks(currFreqTrack),ts);
        end
    else
        currFreqTrack = 0;
        continue;
    end
    chosenTracks(ts) = currFreqTrack;   % save the order of tracks chosen
end

%% for any magnitude value that is below ~-30dB, set the corresponding
% frequency value to 0
for i=1:length(freqTraj)
    if magTraj(i) < 0.02
        freqTraj(i) = 0;
    end
end


%% plot and listen

% final frequency trajectory
figure;
plot(tt, freqTraj, 'linewidth', 2);
xlabel('time (FFT frames)');
ylabel('frequency (Hz)');
title(sprintf('frequency trajectory adjusted using magnitude trajectory'));
grid on

% resynthesize with sine waves
xr1 = synthtrax(freqTraj',magTraj',fs,winSize,winSize/2);
soundsc(x,fs);
pause(length(x)/fs + 0.1);
soundsc(xr1, fs);

dlmwrite([freqOutDir inputName '.txt'], freqTraj);
dlmwrite([magOutDir inputName '.txt'], magTraj);


% % plot 
% figure;
% plot(tt, freqTraj, 'linewidth', 2);
% xlabel('time (FFT frames)');
% ylabel('frequency (Hz)');
% title(sprintf('best frequency track estimate (from combined method)'));
% grid on
% 
% figure;
% plot(chosenTracks, 'linewidth', 2);
% xlabel('time (FFT frames)');
% ylabel('chosen frequency track index');
% title(sprintf('chosen frequency tracks over time, indexed by possible frequency tracks'));
% grid on
% 
% % normalize and plot with magnitude envelope
% fT = (freqTraj - min(freqTraj)) / (max(freqTraj) - min(freqTraj));
% mT = (magTraj - min(magTraj)) / (max(magTraj) - min(magTraj));
% figure;
% plot(tt, fT, 'linewidth', 2);
% hold on
% plot(tt, mT, 'r', 'linewidth', 2);
% xlabel('time (FFT frames)');
% ylabel('frequency (Hz)');
% title(sprintf('best frequency track estimate with magnitude'));
% grid on








