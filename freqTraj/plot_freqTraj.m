% plot spectrogram + frequency tracks
% and frequency trajectory

addpath('sinemodel');

i = 4;

winSize = 1024;
saxMinFreq = 125;
saxMaxFreq = 900;

saxDir = 'gestures/';
saxFiles = dir(saxDir);
saxFiles = saxFiles(3:end);

[x,fs] = wavread([saxDir saxFiles(i).name]);

% cut sound file so that it doesn't have a bunch of zeros in the beginning
x = x(17400:80000);

%% processing to find frequency tracks and frequency trajectory

% phase-preserving analysis 
% spectrogram and instantaneous frequency gram
[I,S]=ifgram(x,winSize,winSize,winSize/2,fs);
% extract the peak tracks based on the new STFT
[R,M]=extractrax(abs(S), 0.1);
% interpolated IF-gram values for exact track frequencies (Hz)
F = colinterpvals(R,I);
tt = [1:size(R,2)] * (winSize/2/fs);

% try to get a frequency trajectory by choosing the 'best' frequency track
nTimeSlices = length(tt);
nTracks = size(F,1);
% for each frequency track
% count occurences of frequencies outside
nFreqsOutside = zeros(nTracks,1);
for track=1:nTracks
    nFreqsOutside(track) = sum(F(track,:) < saxMinFreq) + ...
        sum(F(track,:) > saxMaxFreq);
end

possibleTracks = find(nFreqsOutside == min(nFreqsOutside(:)));
possibleFT = F(possibleTracks,:);

% currFreqTrack will index the correct frequency track in possibleTracks
% so to get the correct frequency track index in F, we have to do
% possibleTracks(currFreqTrack)
currFreqTrack = 0;
freqTraj = zeros(nTimeSlices,1);
magTraj = zeros(nTimeSlices,1);
chosenTracks = zeros(nTimeSlices,1);
% after the first chosen track is done, find the next best one...
for ts=1:nTimeSlices
    % check if any track is not nan
    if sum(~isnan(possibleFT(:,ts))) > 0
        if currFreqTrack ~= 0
            % we are already following a track
            if isnan(possibleFT(currFreqTrack,ts))
                % the current one is nan so we should switch to the next
                [~, mi] = min(possibleFT(:,ts));
                currFreqTrack = mi;
                freqTraj(ts) = F(possibleTracks(currFreqTrack),ts);
                magTraj(ts) = M(possibleTracks(currFreqTrack),ts);
            else
                % the current one is not nan, keep following it
                freqTraj(ts) = F(possibleTracks(currFreqTrack),ts);
                magTraj(ts) = M(possibleTracks(currFreqTrack),ts);
            end
        else
            % we are currently not following a track 
            % choose a new track (the one with lowest freq)
            [~, mi] = min(possibleFT(:,ts));
            currFreqTrack = mi;
            freqTraj(ts) = F(possibleTracks(currFreqTrack),ts);
            magTraj(ts) = M(possibleTracks(currFreqTrack),ts);
        end
    else
        currFreqTrack = 0;
        continue;
    end
    chosenTracks(ts) = currFreqTrack;   % save the order of tracks chosen
end

% for any magnitude value that is below ~-30dB, set the corresponding
% frequency value to 0
freqTrajNO = freqTraj;
for j=1:length(freqTraj)
    if magTraj(j) < 0.02
        freqTraj(j) = 0;
    end
end

    
%% Plot fix with 'zoom' showing only the red frequency tracks

% frequency tracks

tracksUsed = unique(chosenTracks);
tracksUsed = tracksUsed(tracksUsed ~= 0);
tracksUsed = possibleTracks(tracksUsed);

figure;
subplot(211);
specgram(x, winSize, fs);
colormap(1-gray)
hold on
Fp = F';
hdft = plot(tt, Fp(:,tracksUsed), 'white', 'linewidth', 3);
hft = plot(tt, F', 'r', 'linewidth', 1);
legend(hft, 'frequency tracks');
xlabel('time (FFT frames)', 'FontSize', 11);
ylabel('frequency (Hz)', 'FontSize', 11);
title(sprintf('spectrogram of saxophone gesture and frequency tracks'), 'FontSize', 11);
xlim([0 1.2])
ylim([0 5500])

% frequency trajectory
subplot(212);
plot(tt, freqTraj, 'k', 'linewidth', 2);
xlabel('time (FFT frames)', 'FontSize', 11);
ylabel('frequency (Hz)', 'FontSize', 11);
title(sprintf('frequency trajectory'), 'FontSize', 11);
xlim([0 1.2])
grid on


