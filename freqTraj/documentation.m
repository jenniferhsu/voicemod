%% Sinusoidal modeling for determining saxophone frequency trajectory - documentation
% This document contains my experiments on using sinusoidal modeling techniques 
% to determine a saxophone frequency trajectory to use as a gestural input to a 
% physical voice model.  We use a sinusoidal model implementation in Matlab, 
% Sinemodel (Ellis, http://www.ee.columbia.edu/ln/rosa/matlab/sinemodel/),
% to extract frequency tracks from the saxophone input.  

%% Problem introduction
% We want to take a saxophone gesture as input, and determine the frequency 
% trajectory of the gesture.  Current objects in Pd do not match our 
% perceptual expectations of the gesture because of outliers. Please see
% the plots in 'Output from Pd objects.' The first solution is to use a
% short-time Fourier transform (STFT) and for each time frame, pick the
% frequency of the peak with the largest magnitude.  This solution does not
% work very well because the peak with the largest magnitude is not the
% peak for the fundamental frequency.  A better solution is to track peaks
% over time and get multiple frequency tracks.  Then we narrow down the
% number of frequency tracks by pruning out the tracks that include
% frequencies that the saxophone cannot produce.  We can then choose the
% frequency track with the lowest frequency -- this is usually the track
% that contains the fundamental frequency of the frequency trajectory.
% This solution can be extended to include more than one frequency track as
% shown in the 'Dealing with frequency track switching' section.  Last,
% there is a problem with the frequency trajectory increasing in frequency
% during the release of a gesture in many of the examples.  Using the
% magnitude trajectory, we can adjust the frequency trajectory so that it
% matches our expectations.


%% Output from Pd objects
% Pd object (fiddle~ and sigmund~) frequency trajectory settings
% * fiddle~ 1024 pt FFT window, 512 overlap, cutoff at 40dB
% * sigmund~ 1024 pt FFT window, cutoff at 40dB, cutoff at 900Hz
%%
% fiddle~ and sigmund~ code/plots:

saxDir = 'gestures/';
saxFiles = dir(saxDir);
saxFiles = saxFiles(3:end);

fiddleDir = 'pd_fiddle_output/';
fiddleFiles = dir(fiddleDir);
fiddleFiles = fiddleFiles(3:end);

sigmundDir = 'pd_sigmund_output/';
sigmundFiles = dir(sigmundDir);
sigmundFiles = sigmundFiles(3:end);

for i=1:8

    ft = textread([fiddleDir fiddleFiles(i).name]);
    st = textread([sigmundDir sigmundFiles(i).name]);
    
    figure;
    subplot(211);
    plot(ft, 'linewidth', 2);
    ylim([0 2100]);
    grid on
    title(sprintf('fiddle output for %s', saxFiles(i).name));
    xlabel('time (FFT frames)');
    ylabel('frequency (Hz)');
    
    subplot(212);
    plot(st, 'linewidth', 2);
    ylim([0 2100]);
    grid on
    title(sprintf('sigmund output for gesture %s', saxFiles(i).name));
    xlabel('time (FFT frames)');
    ylabel('frequency (Hz)');

end

%% First solution: short-time Fourier Transform (STFT) and picking the largest peak
% Method: Perform a STFT on input saxophone sound. 
%%
% For each time frame, we find 
% the peak (local maximum) with the highest amplitude in the magnitude
% spectrum. The frequency trajectory is the saved frequencies of each 
% peak for each time frame. This solution does not work very well, because 
% the output frequency trajectory jumps between frequencies too much.
%%
% The following code shows how this solution works for just one sound
% example.  Note that there may be mistakes in the code (and that could be
% the reason for why the solution does not work well).
% In this code, we use an FFT window size of 1024, 512 sample hop, and cut off
% frequencies in output to the frequency range of the saxophone.
%%
% (see smsForFreqTrajv01.m)

[x, fs] = wavread('gestures/gesture3.wav');

M = 1024; % may need to make this odd?
w = hanning(M);
N = M;
R = M/2;
nframes = floor(length(x)/R)-1;

freqMin = 125;       % minimum frequency produced by saxophone
freqMax = 900+200;   % maximum freq produced by saxophone + some extra range
binMin = floor(freqMin*N/fs);
binMax = floor(freqMax*N/fs);
magThresh = -40;        % don't record frequency if it is less than 40dB

Xtwz = zeros(N,nframes); % pre-allocate STFT output array
M = length(w);           % M = window length, N = FFT length
zp = zeros(N-M,1);       % zero padding (to be inserted)
xoff = 0;                % current offset in input signal x

F = zeros(nframes, 1);
h = figure;
maxInds = [];

faxis = fs/(2*pi)*linspace(0, pi, N/2);

for m=1:nframes

  
  xt = x(xoff+1:xoff+M); % extract frame of input data
  xtw = w .* xt;         % apply window to current frame
  xtwz = xtw;
  Xtwz(:,m) = fftshift(fft(ifftshift(xtwz),N)); % STFT for frame m
  
  % pick maximum peak
  Xtwzabs = abs(Xtwz(:,m));
  % disregard negative part
  Xtwzabs = Xtwzabs((N/2)+1:end);
  
  peakInds = [];
  peakMags = [];
  for i=binMin:binMax
      if Xtwzabs(i) > Xtwzabs(i-1) && Xtwzabs(i) > Xtwzabs(i+1)
          peakInds = [peakInds i];
          peakMags = [peakMags Xtwzabs(i)];
      end
  end

  [peakMax, mi] = max(peakMags);
  % cut out peak if it is below a threshold value
  magDB = 20*log10(peakMax);
  if magDB < magThresh
    maxInds = [maxInds 0];
    peakInd = 0;
    F(m) = 0;
  else
    peakInd = peakInds(mi);
    maxInds = [maxInds peakInd];
    F(m) = faxis(peakInd);
  end
  
  % advance pointer by hop-size R
  xoff = xoff + R;       
  
end

plot(F, 'linewidth', 2);
title('frequency trajectory from picking largest peak in each STFT frame for gesture3.wav')
xlabel('time (fft frame)');
ylabel('frequency (Hz)');
ylim([0 1300]);
grid on


%% Better solution: use frequency tracks to determine the frequency trajectory
% This method uses Dan Ellis's sinemodel code to pick out peaks and find
% frequency tracks.  Surprisingly, choosing the frequency track with maximum 
% amplitude does not give good results.  It gives us a track that is much higher
% in frequency than the perceived fundamental frequency of the saxophone
% gesture.  
%%
% Instead, we first minimize the possible frequency tracks to the 
% range of frequencies that the saxophone can produce. Then we look through
% those candidate frequency tracks for the best fitting frequency track.
% For now, we just choose the track with the lowest frequencies.  This
% should be updated later to choose the "best" (what do we mean by "best"?)
% frequency track out of a few possible frequency tracks and also a scheme
% to continue to another frequency track once the first one is done in
% time.  I think we should stick with one frequency track until it dies
% out, and then move to another frequency track after that.
% Note that the threshold value for finding tracks is set pretty high
% (0.1).
%%
% (see sinemodel_maxMagFreqTrackv03.m)

addpath('sinemodel');

winSize = 1024;
saxMinFreq = 125;
saxMaxFreq = 900;

saxDir = 'gestures/';
saxFiles = dir(saxDir);
saxFiles = saxFiles(3:end);

for i=1:length(saxFiles)
    
    [x,fs] = wavread([saxDir saxFiles(i).name]);

    % track analysis
    S = specgram(x, winSize);
    [R,M] = extractrax(abs(S), 0.1);
    tt = [1:size(R,2)] * (winSize/2/fs);
    F = R*fs/winSize;   % go from bins to frequency

    % plot tracks found
    figure;
    subplot(211);
    specgram(x, winSize, fs);
    colormap(1-gray)
    hold on
    plot(tt, F', 'r');
    legend('frequency tracks');
    xlabel('time (fft frame)');
    ylabel('frequency (Hz)');
    title(sprintf('spectrogram of sax file %s and frequency tracks', saxFiles(i).name));

    % try to get a frequency trajectory by choosing the 'best' frequency track
    % for each frequency track, count occurences of frequencies outside
    nTracks = size(F,1);
    nFreqsOutside = zeros(nTracks,1);
    for track=1:nTracks
        nFreqsOutside(track) = sum(F(track,:) < saxMinFreq) + ...
            sum(F(track,:) > saxMaxFreq);
    end

    % choose the frequency track that has the least number of frequencies
    % outside of the saxophone freq range
    [nFreqsOutBest, bestTrack] = min(nFreqsOutside);
    freqTraj = F(bestTrack,:);
    freqTraj(isnan(freqTraj)) = 0;
    magTraj = M(bestTrack,:);

    % plotting
    subplot(212);
    plot(tt, freqTraj, 'linewidth', 2);
    ylim([0, saxMaxFreq+200]);
    grid on
    xlabel('time (FFT frames)');
    ylabel('frequency (Hz)');
    title(sprintf('frequency track for %s', saxFiles(i).name));

    fprintf(1, '\n\n');
    fprintf(1, '%s\n', [saxDir saxFiles(i).name]);
    fprintf(1, 'nFreqsOutBest: %d\n', nFreqsOutBest);
    fprintf(1, 'best track: %d\n', bestTrack);
    

end

%% Dealing with frequency track switching
% The above solution performed reasonably well for most of the gestures.
% The one gesture that does not do as well for is gesture3.wav. It only
% captures the first half of the frequency trajectory.  This part is an
% attempt to fix that problem, and this solution may be used to
% continuously choose the next best frequency track one the current
% frequency track has died.
%%
% This method follows the above method and picks out all the frequency
% tracks that contain frequencies that are within the saxophone range.
% Then it looks through each FFT time frame and looks to see if there
% exists a frequency track for that time frame. If there are frequency
% tracks for this time frame, the algorithm chooses to do one of the following:
%%
% * choose a new track with the lowest frequency value of the possible
% tracks for this time frame (if a frequency track is not currently being 
% used) 
% * continue with this current frequency track if no other frequency 
% tracks have lower frequencies 
% * choose a track with a lower frequency than the current
% frequency track
%%
% We choose tracks that have the lowest frequencies
% because it seems to match the frequency of the saxophone best. It seems
% that the magnitude of each frequency track does not give us an indication
% of the fundamental frequency.  The maximum magnitude frequency track
% usually picks out a track that is way too high in frequency. 
%%
% (see sinemodel_maxMagFreqTrackv04.m)

addpath('sinemodel');

winSize = 1024;
saxMinFreq = 125;
saxMaxFreq = 900;

saxDir = 'gestures/';
saxFiles = dir(saxDir);
saxFiles = saxFiles(3:end);

for i=1:length(saxFiles)

    [x,fs] = wavread([saxDir saxFiles(i).name]);
    
    % analysis
    S = specgram(x, winSize);
    [R,M] = extractrax(abs(S), 0.1);
    tt = [1:size(R,2)] * (winSize/2/fs);
    F = R*fs/winSize;
    
    figure;
    subplot(211);
    specgram(x, winSize, fs);
    colormap(1-gray)
    hold on
    plot(tt, F', 'r');
    legend('frequency tracks');
    xlabel('time (fft frame)');
    ylabel('frequency (Hz)');
    title(sprintf('spectrogram of sax file %s and frequency tracks', saxFiles(i).name));

    % try to get a frequency trajectory by choosing the 'best' frequency track
    nTimeSlices = length(tt);
    freqTraj = zeros(1,nTimeSlices);
    binTraj = zeros(1,nTimeSlices);
    magTraj = zeros(1, nTimeSlices);

    nTracks = size(F,1);
    % for each frequency track
    % count occurences of frequencies outside
    nFreqsOutside = zeros(nTracks,1);
    for track=1:nTracks
        nFreqsOutside(track) = sum(F(track,:) < saxMinFreq) + ...
            sum(F(track,:) > saxMaxFreq);
    end

    possibleTracks = find(nFreqsOutside == min(nFreqsOutside(:)));
    possibleFT = F(possibleTracks,:);

    % currFreqTrack will index the correct frequency track in possibleTracks
    % so to get the correct frequency track index in F, we have to do
    % possibleTracks(currFreqTrack)
    currFreqTrack = 0;
    freqTraj = zeros(nTimeSlices,1);
    magTraj = zeros(nTimeSlices,1);
    chosenTracks = zeros(nTimeSlices,1);
    % after the first chosen track is done, find the next best one...
    for ts=1:nTimeSlices
        % check if any track is not nan
        if sum(~isnan(possibleFT(:,ts))) > 0
            if currFreqTrack ~= 0
                % we are already following a track
                if isnan(possibleFT(currFreqTrack,ts))
                    % the current one is nan so we should switch to the next
                    [~, mi] = min(possibleFT(:,ts));
                    currFreqTrack = mi;
                    freqTraj(ts) = F(possibleTracks(currFreqTrack),ts);
                    magTraj(ts) = M(possibleTracks(currFreqTrack),ts);
                else
                    % the current one is not nan, keep following it
                    freqTraj(ts) = F(possibleTracks(currFreqTrack),ts);
                    magTraj(ts) = M(possibleTracks(currFreqTrack),ts);
                end
            else
                % we are currently not following a track 
                % choose a new track (the one with lowest freq)
                [~, mi] = min(possibleFT(:,ts));
                currFreqTrack = mi;
                freqTraj(ts) = F(possibleTracks(currFreqTrack),ts);
                magTraj(ts) = M(possibleTracks(currFreqTrack),ts);
            end
        else
            currFreqTrack = 0;
            continue;
        end
        chosenTracks(ts) = currFreqTrack;   % save the order of tracks chosen
    end

    % plot 
    subplot(212);
    plot(tt, freqTraj, 'linewidth', 2);
    xlabel('time (FFT frames)');
    ylabel('frequency (Hz)');
    title(sprintf('best frequency track estimate for %s (from combined method)', saxFiles(i).name));
    grid on

end

%% Errors in the release of the saxophone note
% In the frequency trajectories for gesture 0, 1, 2, 4, 5, and 7, there is
% an extra spike in frequency near the end of the frequency trajectory
% (right before the trajectory returns to 0).  This is not desired because
% it does not match what we expect from the sound of the saxophone gesture.
%%
% In my first experiment, I tried to compare the energy in the sinusoidal
% model with the energy in the noise residual to see if this was a problem
% with a noisy release.  It doesn't seem to be related to this.  It seems
% that the frequency tracking is picking out the correct frequencies, but
% the magnitude of the frequency trajectory at that time falls below the
% hearing threshold.  We can look at the magnitude trajectory for each time
% frame and any time the magnitude is below a certain value (~-30dB), we
% set the frequency trajectory at that time frame to 0.
%% 
% Please see the code and plots below. Note that we are doing the analysis
% with an instantaneous frequency gram (from Dan Ellis's tutorial).  This
% just gives magnitude scaled to the correct values for adjusting the
% frequency trajectory.
%%
% (see sinemodel_maxMagFreqTrackv06.m)

addpath('sinemodel');

winSize = 1024;
saxMinFreq = 125;
saxMaxFreq = 900;

saxDir = 'gestures/';
saxFiles = dir(saxDir);
saxFiles = saxFiles(3:end);

for i=1:length(saxFiles)

    [x,fs] = wavread([saxDir saxFiles(i).name]);

    % phase-preserving analysis 
    % spectrogram and instantaneous frequency gram
    [I,S]=ifgram(x,winSize,winSize,winSize/2,fs);
    % extract the peak tracks based on the new STFT
    [R,M]=extractrax(abs(S), 0.1);
    % interpolated IF-gram values for exact track frequencies (Hz)
    F = colinterpvals(R,I);
    tt = [1:size(R,2)] * (winSize/2/fs);

    % try to get a frequency trajectory by choosing the 'best' frequency track
    nTimeSlices = length(tt);
    nTracks = size(F,1);
    % for each frequency track
    % count occurences of frequencies outside
    nFreqsOutside = zeros(nTracks,1);
    for track=1:nTracks
        nFreqsOutside(track) = sum(F(track,:) < saxMinFreq) + ...
            sum(F(track,:) > saxMaxFreq);
    end

    possibleTracks = find(nFreqsOutside == min(nFreqsOutside(:)));
    possibleFT = F(possibleTracks,:);

    % currFreqTrack will index the correct frequency track in possibleTracks
    % so to get the correct frequency track index in F, we have to do
    % possibleTracks(currFreqTrack)
    currFreqTrack = 0;
    freqTraj = zeros(nTimeSlices,1);
    magTraj = zeros(nTimeSlices,1);
    chosenTracks = zeros(nTimeSlices,1);
    % after the first chosen track is done, find the next best one...
    for ts=1:nTimeSlices
        % check if any track is not nan
        if sum(~isnan(possibleFT(:,ts))) > 0
            if currFreqTrack ~= 0
                % we are already following a track
                if isnan(possibleFT(currFreqTrack,ts))
                    % the current one is nan so we should switch to the next
                    [~, mi] = min(possibleFT(:,ts));
                    currFreqTrack = mi;
                    freqTraj(ts) = F(possibleTracks(currFreqTrack),ts);
                    magTraj(ts) = M(possibleTracks(currFreqTrack),ts);
                else
                    % the current one is not nan, keep following it
                    freqTraj(ts) = F(possibleTracks(currFreqTrack),ts);
                    magTraj(ts) = M(possibleTracks(currFreqTrack),ts);
                end
            else
                % we are currently not following a track 
                % choose a new track (the one with lowest freq)
                [~, mi] = min(possibleFT(:,ts));
                currFreqTrack = mi;
                freqTraj(ts) = F(possibleTracks(currFreqTrack),ts);
                magTraj(ts) = M(possibleTracks(currFreqTrack),ts);
            end
        else
            currFreqTrack = 0;
            continue;
        end
        chosenTracks(ts) = currFreqTrack;   % save the order of tracks chosen
    end

    % for any magnitude value that is below ~-30dB, set the corresponding
    % frequency value to 0
    freqTrajNO = freqTraj;
    for j=1:length(freqTraj)
        if magTraj(j) < 0.02
            freqTraj(j) = 0;
        end
    end
    
    % plot frequency trajectory without magnitude adjustment
    figure;
    subplot(211);
    plot(tt, freqTrajNO, 'linewidth', 2);
    xlabel('time (FFT frames)');
    ylabel('frequency (Hz)');
    title(sprintf('frequency trajectory NOT adjusted: %s', saxFiles(i).name));
    grid on

    % plot final frequency trajectory
    subplot(212);
    plot(tt, freqTraj, 'linewidth', 2);
    xlabel('time (FFT frames)');
    ylabel('frequency (Hz)');
    title(sprintf('frequency trajectory adjusted using magnitude trajectory: %s', saxFiles(i).name'));
    grid on
    
end
