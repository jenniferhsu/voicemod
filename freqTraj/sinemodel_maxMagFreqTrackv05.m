addpath('sinemodel');

%%%%%%%%%%%%%%%%
% pick out peak tracks
% look through all tracks at each time slice
% pick frequency of track that has the largest magnitude
% only pick frequency tracks that do not include frequencies
%   outside of the saxophone frequency range
% then combine the frequency tracks that have all frequencies
%   in the saxophone range (how to combine?)
% 
% working OK -- just one doesn't work the best i think gesture4.wav
%   because it goes back up
% ideas for improvement:
%   wait it's not my problem, the problem is in the
%       found frequency track from Dan Ellis's code
%   but let's see...is it noisy there? maybe we can choose to not
%       put out anything if it is noisy there...kind of like attacks
%       some weirdly noisy release?
%   ... well that idea didn't work, but it's in this code
%   instead, we look at the magnitude and zero out the frequency
%   when the magnitude is below a certain threshold (~-30dB)
%   this way, the final frequency trajectory looks like what we hear
%   (because we can't hear stuff that is super low in magnitude anyway)
%%%%%%%%%%%%%%%%


% set window size (do tests for different window sizes!)
winSize = 1024;

saxMinFreq = 125;
saxMaxFreq = 900;

% [x, fs] = wavread('gestures/gesture0.wav');
% [x, fs] = wavread('gestures/gesture1.wav');
% [x, fs] = wavread('gestures/gesture2.wav');
% [x, fs] = wavread('gestures/gesture3.wav');
 [x, fs] = wavread('gestures/gesture4.wav');
% [x, fs] = wavread('gestures/gesture5.wav');
% [x, fs] = wavread('gestures/gesture6.wav');
% [x, fs] = wavread('gestures/gesture7.wav');

% analysis

S = specgram(x, winSize);
[R,M] = extractrax(abs(S), 0.1);
tt = [1:size(R,2)] * (winSize/2/fs);

F = R*fs/winSize;
specgram(x, winSize, fs);
colormap(1-gray)
hold on
plot(tt, F', 'r');


%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%

% better analysis/synthesis from Dan Ellis's tutorial page

% Calculate spectrogram and inst.frq.-gram
% with 256 pt FFT & window, 128 pt advance
[I,S]=ifgram(x,winSize,winSize,winSize/2,fs);
% Extract the peak tracks based on the new STFT
[R,M]=extractrax(abs(S), 0.1);
% Calculate the interpolated IF-gram values for exact track frequencies
F = colinterpvals(R,I);
% Interpolate the (columnwise unwrapped) STFT phases to get exact peak
% phases for every sample point (synthesis phase is negative of analysis)
P = -colinterpvals(R,unwrap(angle(S)));
fcols = size(F,2);
% Pad each control matrix with an extra column at each end,
% to account for the N-H = H points lost fitting the first & last windows
F = [0*F(:,1),F,0*F(:,end)];
M = [0*M(:,1),M,0*M(:,end)];
P = [0*P(:,1),P,0*P(:,end)];
% (mulitplying nearest column by 0 preserves pattern of NaN values)

% Now, the phase preserving resynthesis:
xr2 = synthphtrax(F,M,P,fs,winSize,winSize/2);
sound(xr2,fs)
% Noise residual is original with harmonic reconstruction subtracted off
xre = x - xr2(1:length(x))';
specgram(xre,winSize,fs);
colormap(1-gray)
caxis([-60 0])
sound(xre,fs)

%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%







% try to get a frequency trajectory by choosing the 'best' frequency track
nTimeSlices = length(tt);
freqTraj = zeros(1,nTimeSlices);
binTraj = zeros(1,nTimeSlices);
magTraj = zeros(1, nTimeSlices);

nTracks = size(F,1);
% for each frequency track
% count occurences of frequencies outside
nFreqsOutside = zeros(nTracks,1);
for track=1:nTracks
    nFreqsOutside(track) = sum(F(track,:) < saxMinFreq) + ...
        sum(F(track,:) > saxMaxFreq);
end


possibleTracks = find(nFreqsOutside == min(nFreqsOutside(:)));

% the bottom code shows that the magnitudes don't tell us anything 
% about F0 or the frequency track to
% choose:
% for track=1:length(possibleTracks)
%     mags = M(track,:);
%     mags(isnan(mags)) = 0;
%     mean(mags);
% end

possibleFT = F(possibleTracks,:);

% currFreqTrack will index the correct frequency track in possibleTracks
% so to get the correct frequency track index in F, we have to do
% possibleTracks(currFreqTrack)
currFreqTrack = 0;
freqTraj = zeros(nTimeSlices,1);
magTraj = zeros(nTimeSlices,1);
chosenTracks = zeros(nTimeSlices,1);
% after the first chosen track is done, find the next best one...
for ts=1:nTimeSlices
    % check if any track is not nan
    if sum(~isnan(possibleFT(:,ts))) > 0
        if currFreqTrack ~= 0
            % we are already following a track
            if isnan(possibleFT(currFreqTrack,ts))
                % the current one is nan so we should switch to the next
                [~, mi] = min(possibleFT(:,ts));
                currFreqTrack = mi;
                freqTraj(ts) = F(possibleTracks(currFreqTrack),ts);
                magTraj(ts) = M(possibleTracks(currFreqTrack),ts);
            else
                % the current one is not nan, keep following it
                freqTraj(ts) = F(possibleTracks(currFreqTrack),ts);
                magTraj(ts) = M(possibleTracks(currFreqTrack),ts);
            end
        else
            % we are currently not following a track 
            % choose a new track (the one with lowest freq)
            [~, mi] = min(possibleFT(:,ts));
            currFreqTrack = mi;
            freqTraj(ts) = F(possibleTracks(currFreqTrack),ts);
            magTraj(ts) = M(possibleTracks(currFreqTrack),ts);
        end
    else
        currFreqTrack = 0;
        continue;
    end
    chosenTracks(ts) = currFreqTrack;   % save the order of tracks chosen
end


% plot 
figure;
plot(tt, freqTraj, 'linewidth', 2);
xlabel('time (FFT frames)');
ylabel('frequency (Hz)');
title(sprintf('best frequency track estimate (from combined method)'));
grid on

figure;
plot(chosenTracks, 'linewidth', 2);
xlabel('time (FFT frames)');
ylabel('chosen frequency track index');
title(sprintf('chosen frequency tracks over time, indexed by possible frequency tracks'));
grid on

% normalize and plot with magnitude envelope
fT = (freqTraj - min(freqTraj)) / (max(freqTraj) - min(freqTraj));
mT = (magTraj - min(magTraj)) / (max(magTraj) - min(magTraj));
figure;
plot(tt, fT, 'linewidth', 2);
hold on
plot(tt, mT, 'r', 'linewidth', 2);
xlabel('time (FFT frames)');
ylabel('frequency (Hz)');
title(sprintf('best frequency track estimate with magnitude'));
grid on


for i=1:length(freqTraj)
    if magTraj(i) < 0.02
        freqTraj(i) = 0;
    end
end

% ok the above image shows that the magnitude goes way down at the end
% maybe the frequency is there, but we can't hear it? 
% maybe this is kind of like a release where we can ignore what the
% frequency is?
% something like, if the magnitude is below a certain value, don't copy
% down that frequency?

% plot adjusted freqTraj where magTrajvalues below 0.1 zero out the
% frequency trajectory point
figure;
plot(tt, freqTraj, 'linewidth', 2);
xlabel('time (FFT frames)');
ylabel('frequency (Hz)');
title(sprintf('best adjusted frequency track estimate (from combined method)'));
grid on

% resynthesize with sine waves
xr1 = synthtrax(freqTraj',magTraj',fs,winSize,winSize/2);
soundsc(x,fs);
pause(length(x)/fs + 0.1);
soundsc(xr1, fs);



%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%

% now let's compare the energy of residual to the sound?
eWin = 1024;
nIter = floor(length(xre) / (eWin/2)) - 2;
xstart = 1;
indSignalGreater = [];
rats = zeros(nIter,1);
for i=1:nIter
    xreFrame = xre(xstart:xstart+eWin);
    reE = sum(abs(xreFrame).^2);
    xr2Frame = xr2(xstart:xstart+eWin);
    xr2E = sum(abs(xr2Frame).^2);
    rat = 0;
    if xr2E == 0
        rat = reE/eps;
    else
        rat = reE/xr2E;
    end
    rats(i) = rat;
    
    if rat < 1
        indSignalGreater = [indSignalGreater i];
    end
    
    xstart = xstart + floor(eWin/2) + 1;
end
figure;
rats = (rats-min(rats)) / (max(rats)-min(rats));
fT = (freqTraj-min(freqTraj)) / (max(freqTraj)-min(freqTraj));
plot(rats);
hold on
plot(fT,'r');

% hmm...it doesn't seem like this fixes the problem
% also the phase preserving one doesn't do such a great job...

%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%





