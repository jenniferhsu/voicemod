addpath('sinemodel');

%%%%%%%%%%%%%%%%
% pick out peak tracks
% look through all tracks at each time slice
% pick frequency of track that has the largest magnitude
% working OK 
% ideas for improvement:
%   a smoothing operation
%   pick out attacks/onset?
%%%%%%%%%%%%%%%%


% set window size (do tests for different window sizes!)
winSize = 1024;

% [x, fs] = wavread('gestures/gesture0.wav');
% [x, fs] = wavread('gestures/gesture1.wav');
% [x, fs] = wavread('gestures/gesture2.wav');
% [x, fs] = wavread('gestures/gesture3.wav');
% [x, fs] = wavread('gestures/gesture4.wav');
% [x, fs] = wavread('gestures/gesture5.wav');
% [x, fs] = wavread('gestures/gesture6.wav');
 [x, fs] = wavread('gestures/gesture7.wav');



% analysis

S = specgram(x, winSize);
[R,M] = extractrax(abs(S));
disp(['size of R is ', num2str(size(R,1))]);
tt = [1:size(R,2)] * (winSize/2/fs);

F = R*fs/winSize;
specgram(x, winSize, fs);
colormap(1-gray)
hold on
plot(tt, F', 'r');

% try to get max magnitude frequency trajectory
nTimeSlices = length(tt);
freqTraj = zeros(1,nTimeSlices);
binTraj = zeros(1,nTimeSlices);
magTraj = zeros(1, nTimeSlices);
for ts=1:nTimeSlices
    mags = M(:,ts);
    [m, maxFreqBin] = max(mags);
    if isnan(m)
        m = 0;
        magTraj(ts) = m;
        maxFreqBin = 1;
        freqTraj(ts) = 0;
        binTraj(ts) = 0;
    else
        magTraj(ts) = m;
        freqTraj(ts) = F(maxFreqBin, ts);
        binTraj(ts) = maxFreqBin;
    end
end

% plot
figure
%imagesc(M)
%set(gca,'YDir','normal')
%specgram(x, winSize, fs);
%colormap(1-gray)
%hold on
%plot(binTraj, 'r')
% scale the frequencies to match M?
plot(tt,freqTraj, 'r');
xlabel('time (FFT frame)');
ylabel('frequency track');
title('chosen maximum amplitude frequency track');
legend('chosen frequency track');


% resynthesize with sine waves
xr1 = synthtrax(freqTraj,magTraj,fs,winSize,winSize/2);
soundsc(x,fs);
pause(length(x)/fs + 0.1);
soundsc(xr1, fs);


