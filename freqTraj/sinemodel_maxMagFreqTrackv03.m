addpath('sinemodel');

%%%%%%%%%%%%%%%%
% pick out peak tracks
% look through all tracks at each time slice
% pick frequency of track that has the largest magnitude
% only pick frequency tracks that do not include frequencies
%   outside of the saxophone frequency range
%   ==> note, this just chooses one frequency track that has frequencies
%       within the range of the saxophone
%       there may be other tracks that have all frequencies within
%       the saxophone range
%   ==> for that...see sinemodel_maxMagFreqTrackv04
% working OK 
% ideas for improvement:
%   a smoothing operation
%   pick out attacks/onset?
%%%%%%%%%%%%%%%%


% set window size (do tests for different window sizes!)
winSize = 1024;

saxMinFreq = 125;
saxMaxFreq = 900;

% [x, fs] = wavread('gestures/gesture0.wav');
% [x, fs] = wavread('gestures/gesture1.wav');
% [x, fs] = wavread('gestures/gesture2.wav');
 [x, fs] = wavread('gestures/gesture3.wav');
% [x, fs] = wavread('gestures/gesture4.wav');
% [x, fs] = wavread('gestures/gesture5.wav');
% [x, fs] = wavread('gestures/gesture6.wav');
% [x, fs] = wavread('gestures/gesture7.wav');

% analysis

S = specgram(x, winSize);
[R,M] = extractrax(abs(S), 0.1);
tt = [1:size(R,2)] * (winSize/2/fs);

F = R*fs/winSize;
specgram(x, winSize, fs);
colormap(1-gray)
hold on
plot(tt, F', 'r');

% try to get a frequency trajectory by choosing the 'best' frequency track
nTimeSlices = length(tt);
freqTraj = zeros(1,nTimeSlices);
binTraj = zeros(1,nTimeSlices);
magTraj = zeros(1, nTimeSlices);

nTracks = size(F,1);
% for each frequency track
% count occurences of frequencies outside
nFreqsOutside = zeros(nTracks,1);
for track=1:nTracks
    nFreqsOutside(track) = sum(F(track,:) < saxMinFreq) + ...
        sum(F(track,:) > saxMaxFreq);
end
[nFreqsOutBest, bestTrack] = min(nFreqsOutside);
freqTraj = F(bestTrack,:);
freqTraj(isnan(freqTraj)) = 0;
magTraj = M(bestTrack,:);

% plot it
figure;
plot(tt, freqTraj, 'linewidth', 2);
xlabel('time (FFT frames)');
ylabel('frequency (Hz)');
title(sprintf('best frequency track with %d frequencies outside of sax range', nFreqsOutBest));

% print some diagnostics
fprintf(1, '\n\n');
fprintf(1, 'nFreqsOutBest: %d\n', nFreqsOutBest);
fprintf(1, 'best track: %d\n', bestTrack);


% resynthesize with sine waves
xr1 = synthtrax(freqTraj,magTraj,fs,winSize,winSize/2);
soundsc(x,fs);
pause(length(x)/fs + 0.1);
soundsc(xr1, fs);


