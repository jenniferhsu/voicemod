% create frequency tables version 4
% cut off tension values at 8000

v4Settings

% save sounding f0
f0SoundMat = zeros(length(p), length(k));

% iterate through function
for i=1:length(p)
    
	% for each pressure value 
    fprintf(1, 'synthesizing signals for pressure = %f\n', p(i));
    pm = p(i)*ones(N,1);

    for j=1:length(k)
        % for each tension value
        
        % synthesize a sound with chosen pressure and tension
        y = voiceModelv02(k(j), pm, fs);
        
        % analyze for fundamental frequency of synthesis
        S = specgram(y, winSize);
        [R,M] = extractrax(abs(S), 0.1);
        F = R*fs/winSize;   % go from bins to frequency

        % fixes
%         if (j==2 && i==8) ...
%                 || (j==3 && (i==3 || i==4 || i==9 || i==10)) ...
%                 || (j==4 && i==1)
%             mf = min(F);
%             f0SoundMat(i,j) = min(mf(2:end));
%         elseif (j==65 && i==6) || (j==66 && i==3) || (j==67 && i==3) ...
%                || (j==68 && i==2) || (j==68 && i==10) 
%             f0SoundMat(i,j) = max(F(2,:));
%         else
%             % most fall under this 
%             f0SoundMat(i,j) = min(min(F));
%         end
% 
%             % test with sin wave
%             f = min(min(F));
%             tt = 0:1/fs:(0.5-(1/fs));
%             ysin = sin(2*pi*f*tt);
%             fprintf('frequency chosen: %f\n', f);
%             soundsc(ysin,fs);
%             pause(length(ysin)/fs + 0.1);
%             soundsc(y,fs);
%             keyboard
    
        if (j==3 && (i==5 || i==6 || i==7 || i==12 || i==16 || i==17 || i==18 || i==19)) ...%(i==1 && j==4)
           || (j==4 && (i==1 || i==2)) ...
           || (j==2 && i==15)
            mf = min(F);
            f = min(mf(2:end));
            f0SoundMat(i,j) = f;
        else
            f0SoundMat(i,j) = min(min(F));
        end
       
%        wavwrite(y, fs, ['tableSounds/p' num2str(p(i)) '_k' num2str(k(j)) '.wav']);
        
    end
end

%% plot
figure
imagesc(p, k, flipud(f0SoundMat));
set(gca,'YDir','normal');
colorbar
xlabel('tension');
ylabel('pressure (Pa)');
title('sounding frequency of different pressure/tension combinations');

% there are definitely some mistakes:
figure
plot(f0SoundMat)

% now to see the relationship between the frequency that we input
% and the frequency that sounds:
f0(1) 
f0SoundMat(:,1)
% the above line is for all different pressure values,
% how does changing the reed frequency/reed tension change the sounding
% frequency?

% they all seem to have a similar shape
%% maybe can do a parabolic fit to this?
for i=1:length(k)
    plot(p, f0SoundMat(:,i));
    title(sprintf('freqInd: %d', i));
    pause(0.75);
end

plot(f0SoundMat)


% for j=20
%     for i=1:length(p)
%   
%     pm = p(i)*ones(N,1);
%     
%     y = voiceModelv02(k(j), pm, fs);
%     
%     range = max(f0SoundMat(:,j)) - min(f0SoundMat(:,j));
%     fprintf('j=%d, range=%f\n', j, range);
%     
%     % test with sin wave
%     f = f0SoundMat(i,j);
%      S = specgram(y, winSize);
%         [R,M] = extractrax(abs(S), 0.1);
%         F = R*fs/winSize;   % go from bins to frequency
%         
%     f = max(min(F));    
%     tt = 0:1/fs:(0.5-(1/fs));
%     ysin = sin(2*pi*f*tt);
%     fprintf('frequency chosen: %f\n', f);
%     soundsc(ysin,fs);
%     pause(length(ysin)/fs + 0.1);
%     soundsc(y,fs);
%     keyboard
%     
%     end
% end
