%% This script is for creating different sounds using the new voice model
% that outputs a voice sound given a sounding frequency and pressure
% (voiceModelv04.m)

v4Settings
load f0SoundMatV4.mat
load coeffsV4.mat

fs = 44100; % sampling rate
N = 44100;  % duration of final signal

% specify two sounding frequencies amd blowing pressure
f1 = 220.0;
f2 = 248.0;
bp1 = 800;
bp2 = 2000;

%% Case 1: desired sound frequency changes, but pressure can stay the same

% create sounding frequency and blowing pressure vectors
fVec = [f1*ones(N/2,1); f2*ones(N/2,1)];
bpVec = bp1*ones(N,1);

% call the voice model
y = voiceModelv04(fVec, bpVec, fs, 'secAreaData/A-bart44100.txt');

%% Case 2: desired sound frequency stays the same, but pressure changes

% create sounding frequency and blowing pressure vectors
fVec = f1*ones(N,1);
bpVec = [bp1*ones(N/2,1); bp2*ones(N/2,1)];

% call the voice model
y = voiceModelv04(fVec, bpVec, fs, 'secAreaData/A-bart44100.txt');


%% Case 3: sounding frequency and pressure change

fVec = [f1*ones(N/2,1); f2*ones(N/2,1)];
bpVec = [bp1*ones(N/2,1); bp2*ones(N/2,1)];

% call the voice model
y = voiceModelv04(fVec, bpVec, fs, 'secAreaData/A-bart44100.txt');


%% Case 4: how does it sound with a different vowel sound?

yE = voiceModelv04(fVec, bpVec, fs, 'secAreaData/I-bit44100.txt');


%% Case 5: how does it sound with a frequency trajectory that I 
% extracted from a saxophone gesture?

freqTraj = textread('../freqTraj/freqTrajOutput/gesture3.txt');
freqTraj = freqTraj(freqTraj~=0);

% compare this with the saxophone gesture
ysax = wavread('../freqTraj/gestures/gesture3.wav');

% the frequency trajectory was analyzed with windows of size 1024 with
% overlap by 1/2, so we need to upsample this frequency trajectory
% to have 511 points between each value

% linearly interpolate to get the frequency values in between each of the
% freqTraj points
ptsInBtwn = 512;
fVec = zeros(length(freqTraj)*ptsInBtwn, 1);
for i=1:length(freqTraj)-1
    x1 = 1;
    x2 = 512;
    y1 = freqTraj(i);
    y2 = freqTraj(i+1);
    m = (y2-y1)/(x2-x1);
    b = y2 - m*x2;  
    fVec(ptsInBtwn*(i-1)+1) = y1;
    for j=1:ptsInBtwn-1
        fVec(ptsInBtwn*(i-1)+1+j) = m*(x1+j) + b;
    end
end

bpVec = bp1*ones(length(fVec),1);
y = voiceModelv04(fVec, bpVec, fs, 'secAreaData/A-bart44100.txt');

soundsc(y, fs);
pause(length(y)/fs + 0.1);
soundsc(ysax, fs);

%% Case 6: same as 5, but do a smooth pressure envelope

freqTraj = textread('../freqTraj/freqTrajOutput/gesture3.txt');
freqTraj = freqTraj(freqTraj~=0);
freqTraj = [1; 50; freqTraj; 50; 1; 0];

% compare this with the saxophone gesture
ysax = wavread('../freqTraj/gestures/gesture3.wav');

% the frequency trajectory was analyzed with windows of size 1024 with
% overlap by 1/2, so we need to upsample this frequency trajectory
% to have 511 points between each value

% linearly interpolate to get the frequency values in between each of the
% freqTraj points
ptsInBtwn = 512;
fVec = zeros(length(freqTraj)*ptsInBtwn, 1);
for i=1:length(freqTraj)-1
    x1 = 1;
    x2 = 512;
    y1 = freqTraj(i);
    y2 = freqTraj(i+1);
    m = (y2-y1)/(x2-x1);
    b = y2 - m*x2;  
    fVec(ptsInBtwn*(i-1)+1) = y1;
    for j=1:ptsInBtwn-1
        fVec(ptsInBtwn*(i-1)+1+j) = m*(x1+j) + b;
    end
end

N = length(fVec);
Nramp = floor(0.2*N);
bpVec = zeros(length(fVec), 1);
% ramp for pressure...may not be necessary
bpVec(23553:67074) = bp1*ones(1,67074-23553+1);
bpVec(23553:24553) = bp1*linspace(0, 1, 24553-23553+1);
y = voiceModelv04(fVec, bpVec, fs, 'secAreaData/A-bart44100.txt');

soundsc(y, fs);
pause(length(y)/fs + 0.1);
soundsc(ysax, fs);

wavwrite(y,fs,'voice_gesture3_freqTraj.wav')