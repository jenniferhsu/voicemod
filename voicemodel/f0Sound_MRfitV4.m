% multiple regression fit for V4

v4Settings
load f0SoundMatV4


%% reconfigure f0SoundMat so that we can use it for multiple regression 
% (or other fits)

Nsamps = length(p)*length(w0);
pressure = zeros(Nsamps,1);
rf = zeros(Nsamps,1);
soundingFreq = zeros(length(p)*length(w0),1);

cnt = 1;
for i=1:length(p)
    for j=1:length(k)
        pressure(cnt) = p(i);
        rf(cnt) = w0(j);
        soundingFreq(cnt) = f0SoundMat(i,j);
        cnt = cnt + 1;
    end
end

X = [ones(Nsamps,1) pressure rf];
[B, BInt, R, RInt, stats] = ...
    regress(soundingFreq, X);


% plot data
scatter3(pressure,rf,soundingFreq,'filled')
xlabel('pressure (Pa)');
ylabel('reed frequency (radians)');
zlabel('sounding freq (Hz)');
% plot fit over it
hold on
x1 = min(pressure):500:max(pressure);
x2 = min(rf):500:max(rf);
[x1fit, x2fit] = meshgrid(x1,x2);
yfit = B(1) + B(2)*x1fit + B(3)*x2fit;
m = mesh(x1fit, x2fit, yfit);
set(m,'facecolor','none');


fprintf(1,'multiple regression F: %f, p-value: %f\n', stats(2), stats(3));

