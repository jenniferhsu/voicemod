%
% synthesize different vowels with the same frequency and glottal pressure
% this is so that we can look at the spectrogram and compare
%
% we want to know, which vowels sound better? why?
% we think that vowels that are farther back may sound slightly worse


v4Settings
load f0SoundMatV4.mat
load coeffsV4.mat

fs = 44100;             % sampling rate
N = 44100;              % duration of final signal
Nfft = 1024;            % FFT samples
Noverlap = 512;         % FFT overlap samples
m = hanning(Nfft);      % FFT window


% specify frequency and blowing pressure
f = 220.0;
bp = 800;

% create frequency and blowing pressure vectors
fVec = f*ones(N,1);
bpVec = bp*ones(N,1);

% get all the cross sectional areas in our folder
saDir = 'secAreaData/';
saFiles = dir(saDir);
saFiles = saFiles(3:end);

y = zeros(length(saFiles), N);

% synthesize a sound per vowel
for i=1:length(saFiles)
    vsynth = voiceModelv04(fVec, bpVec, fs, [saDir saFiles(i).name]);
    y(i,:) = vsynth;
    figure;
    %spectrogram(y(i,:),m,Noverlap,Nfft,fs);
    % choose one slice of the fft to look at since it's steady state
    x = vsynth(1000:1000+Nfft);
    X = fft(x, Nfft);
    Xpos = X(1:Nfft/2);
    Xabs = abs(Xpos);
    semilogx(20*log10(Xabs/max(Xabs)));
    xlabel('fft bin');
    ylabel('magnitude (dB)');
    title(sprintf(saFiles(i).name));
    %saveas(gcf, ['modelVowelPlots/' saFiles(i).name(1:end-4)], 'png');
    close;
end

% play files
input('Press any key when you are ready to listen to the files');
for i=1:length(saFiles)
    fprintf('%s\n', saFiles(i).name);
    soundsc(y(i,:), fs);
    pause((length(y(i,:))/fs) + 0.05);
end



% U-foot44100.txt and u-food44100.txt sound the worst
% i-beet44100.txt doesn't sound that good either
% O-ball44100.txt is a bit muffled sounding
%% OK the solution for this is to set aw to 0.1 instead of 0.14 for these vowels


% ok, so i recorded myself and I am going to look at the difference now
jDir = 'jennifer_vowels/';
jFiles = dir(jDir);
jFiles = jFiles(3:end);

for i=1:length(jFiles)
    
    [vReal,fs] = wavread([jDir jFiles(i).name]);
    vr = vReal(3000:3000+Nfft);
    VR = fft(vr, Nfft);
    VRpos = VR(1:Nfft/2);
    VRabs = abs(VRpos);
   
    figure
    
    subplot(211);
    vsynth = y(i,:);
    vs = vsynth(1000:1000+Nfft);
    VS = fft(vs, Nfft);
    VSpos = VS(1:Nfft/2);
    VSabs = abs(VSpos);
    semilogx(20*log10(VSabs/max(VSabs)));
    xlabel('fft bin');
    ylabel('magnitude (dB)');
    title(sprintf('voice model: %s', saFiles(i).name));
    
    subplot(212);
    semilogx(20*log10(VRabs/max(VRabs)));
    xlabel('fft bin');
    ylabel('magnitude (dB)');
    title(sprintf('voice real: %s', jFiles(i).name));

    %saveas(gcf, ['jenniferVowelPlots/' saFiles(i).name(1:end-4)], 'png');
    close;
    
end

