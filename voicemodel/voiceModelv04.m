function y = voiceModelv04(fVec, pm, fs, secAreaPath)
% Y = VOICEMODELV04( FVEC, PM, FS, secAreaPath )
% is a physical model of the vocal folds and vocal tract
% the function will output a voice sound with the vowel specified by the
% cross sectional area path given as secAreaPath at the specified sounding
% frequency and blowing pressure
%
%
% inputs:
%   - fVec: desired sounding frequency by sample
%   - pm: pressure trajectory by sample
%   - fs: sample rate
%   - secAreaPath: path to cross-sectional area to use
%   note: fr and pm should be the same length
% output:
%   - y: the output signal of length length(fVec)/fs
%
% example call:
%   N = 44100;
%   fs = 44100;
%   f = [220*ones(N/2,1); 233.08*ones(N/2,1)];
%   pm = 800*ones(N,1);
%   y = voiceModelv03(f, pm, fs, 'secAreaData/A-bart44100.txt');
%
%
% about the voice model:
% 	voice with reed model excitation (for the vocal folds)
% 	and 2-port scattering junction waveguide for the vocal tract
%
%
% author: jennifer hsu
% date: winter 2015

    % load matrices saving sounding frequency and w0 curves
    load f0SoundMatV4.mat
    load coeffsV4.mat
    load fitVectors.mat

    N = length(pm);

    SRefl = importdata(secAreaPath);  % cross-sectional area information 
    kRefl = (SRefl(1:end-1) - SRefl(2:end)) ./ (SRefl(1:end-1) + SRefl(2:end)); % reflection coefficient

    M = length(SRefl);  % length of upper/lower rails
    Nj = length(kRefl); % number of junctions
    Ns = length(SRefl); % number of sections

    % vectors to update in loop
    x = zeros(N,1);             % valve opening
    y = zeros(N,1);             % output sound
    in = zeros(N,1);            % input to waveguide
    U = zeros(N,1);             % flow
    F = zeros(N, 1);            % input force       
    Ax = zeros(N,1);            % cross sectional area of valve
    dU = zeros(N,1);            % update for flow

    % upper/lower rails
    upper = zeros(M,1);
    lower = zeros(M,1);

    % physical constants
    rho = 1.2;                  % air density, kg/m^3 
    cSos = 346.3;               % speed of sound, m/s

    % reed variables, physical parameters
    mass = 0.00028;             % mass, kg
    a = 0.014;                 % tube radius, m
    %a = 0.1;                  % tube radius, m (to make U-food, i-beet
    %sound better)
    Z0 = rho*cSos/(pi*a*a);     % characteristic impedance
    aw = 0.0049;                % valve width, m
    l = 0.014;                  % valve length, m
    mu = 0.003;                 % thickness of valve, m
    %mu = 0.001;                 % thickness of valve, m (for less
    %buzziness)
    x0 = .0002;                 % initial displacement, m
    Q = 9;                      % quality factor that determines damping 
    eta = .0007;                % normalized displacement, ratio (?)
    nu = 0.5;                   % displacement shape, exponent (?)
    
    % create kTens vector based on desired sounding frequency and pressure
    
    % find sounding frequency curve closest to specified blowing pressure
    % let's pretend it's a constant blowing pressure for now
    coeffsSamps = zeros(N,2);
    for pInd=1:length(pm)
        [~, pminDistInd] = min((pm(pInd) - fitVectors.p).^2);
        %coeffsSamps(pInd,:) = coeffs(pminDistInd,:); % this is the chosen w0 curve for the given blowing pressure

        if pm(pInd) < max(fitVectors.p) && pm(pInd) > min(fitVectors.p)
            % linearly interpolate the pressure coefficients
            if pm(pInd) - fitVectors.p(pminDistInd) > 0
                xp1 = fitVectors.p(pminDistInd);
                xp2 = fitVectors.p(pminDistInd+1);
                b1 = coeffs(pminDistInd,:);
                b2 = coeffs(pminDistInd+1,:);
            else
                xp1 = fitVectors.p(pminDistInd-1);
                xp2 = fitVectors.p(pminDistInd);
                b1 = coeffs(pminDistInd-1,:);
                b2 = coeffs(pminDistInd,:);
            end
            li_alph = (pm(pInd) - xp1)/(fitVectors.p(2)-fitVectors.p(1));
            coeffsSamps(pInd,:) = (1-li_alph)*b1 + li_alph*b2;
        else
            coeffsSamps(pInd,:) = coeffs(pminDistInd,:);
        end
    end

    % now let's find values of w0 that work for the sounding frequency
    % vector
    w0 = zeros(length(fVec),1);
    kTens = zeros(length(fVec),1);
    for freq=1:length(fVec)
        mc = coeffsSamps(pInd,:);
        Px = [mc(1) mc(end)-fVec(freq)];
        if fVec(freq)==0
            w0(freq) = 0;
        else
            w0(freq) = roots(Px);
        end
        if w0(freq) < 0
            w0(freq) = 0;
        end
        kTens(freq) = mass*w0(freq)*w0(freq);
        
    end

    % initialize valve opening with initial displacement
%    x(2) = x0;   HMMMM maybe I should take this out?

    % pressure input initialization
    p2 = 0;

    % resonant filter coefficients calculations
    c = 2*fs;                   % bilinear transform scalar 2/T
    %g = sqrt(mass*kTens)/Q;
    %a0 = mass*c*c + mass*g*c + kTens;
    %a1 = -2*(mass*c*c - kTens);
    %a2 = mass*c*c - mass*g*c + kTens;

    % reflection coeffs
    R0 = 0.9;
    BL = -[.2162 .2171 .0545];
    AL = [1 -.6032 .0910];
    stateL = [0 0];

    % transition coefs
    BT = AL+BL;
    AT = AL;
    stateT = [0 0];

    % wall loss filters
    bL = [0.806451596106077  -1.855863155228909   1.371191452991298  ...
          -0.312274852426121  -0.006883256612646];
    aL = [1.000000000000000  -2.392436499871960   1.891289981326362  ...
          -0.511406512428537   0.015235504020645];

    stateLU = [0 0 0 0];  % state upper lambda
    stateLL = [0 0 0 0];  % state lower lambda

    stateF = [0 0];

    for n=3:N

        %% excitation
        % pressure difference
        dp = abs(p2 - pm(n));

        % height, cross sectional area, flow
        Ax(n) = aw * l*eta*(abs(x(n-1)/eta)^nu);

        dd = Ax(n)/(mu*rho);
        ee = 1/(2*mu*Ax(n) + U(n-1)/fs + eps);
        dU(n) = (dU(n-1) + dd*dp - ee*U(n-1)*U(n-1))/(2*fs);
        U(n) = U(n-1) + dU(n);

        % calculate filter coefficients
        g = sqrt(mass*kTens(n))/Q;
        a0 = mass*c*c + mass*g*c + kTens(n);
        a1 = -2*(mass*c*c - kTens(n));
        a2 = mass*c*c - mass*g*c + kTens(n);

        % forcing function
        Fm = aw*l*pm(n);
        Fb = -aw*l*p2;

        if Ax(n) > 0
            if x(n-1) > 0
                FU = aw * mu * (pm(n) - ((rho/2.0)*(U(n)/Ax(n))*(U(n)/Ax(n))));
            elseif x(n-1) < 0
                FU = -1*( aw * mu * (pm(n) - ((rho/2.0)*(U(n)/Ax(n))*(U(n)/Ax(n)))));
            else
                FU = 0;
            end
        else
            FU = 0;    
        end

        F(n) = Fm + Fb + FU + kTens(n)*x0;

        % filter
        rfOut = (F(n) + 2*stateF(1) + stateF(2))/a0 - ...
            (a1/a0)*x(n-1) - (a2/a0)*x(n-2);
        stateF(2) = stateF(1);
        stateF(1) = F(n);

        if rfOut < 0
           x(n) = 0;
        else
           x(n) = rfOut;
        end

        in(n) = Z0*U(n);

        %% DWG part - voice dwg

        % save old delay lines
        upperOld = upper;
        lowerOld = lower;

        % read from delay lines
        % wall losses
        [outL, stateLU] = filter(bL, aL, upper(end), stateLU);
        [out0, stateLL] = filter(bL, aL, lower(1), stateLL);
        %outL = upper(end);
        %out0 = lower(1);

        % write to delay lines
        upper(1) = out0*R0 + in(n); 
        [lower(end), stateL] = filter(BL, AL, outL, stateL);

        % clip it!
        if outL > 1
            outL = 1;
        elseif outL < -1
            outL = -1;
        end
        % output
        y(n) = outL;%upper(end); 
        %[y(n), stateT] = filter(BT, AT, upper(end), stateT);

        % shift everything over/push through scattering junctions
        for m=1:Nj
            lower(m) = upperOld(m)*kRefl(m) + lowerOld(m+1)*(1-kRefl(m));
            upper(m+1) = upperOld(m)*(1+kRefl(m)) + lowerOld(m+1)*(-kRefl(m));
        end

        p2 = upper(1)+lower(1);

    end

    % scale output sound
    m = (-1-1)/(min(y) - max(y));
    b = 1 - m*max(y);
    yn = m*y + b;
    yn = 0.9*yn;
    
    y = yn;

end