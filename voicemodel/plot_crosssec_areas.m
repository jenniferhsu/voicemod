S = textread('secAreaData/A-bart44100.txt');

[x,y,z] = cylinder(S/(2*pi));
h = surf(x,y,z);
rotate3d on;

% this code lets you plot the cross sectional area as a cylinder