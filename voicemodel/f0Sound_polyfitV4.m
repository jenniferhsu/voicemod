% polyfit for v4 table

v4Settings
load f0SoundMatV4.mat

% it looks really similar for different pressures...
% let's see if we can average it
coeffs = zeros(length(p), 2);
for i=1:length(p)
    %coeffs(i,:) = polyfit(k,f0SoundMat(i,:),2);
    coeffs(i,:) = polyfit(w0,f0SoundMat(i,:),1);
end

% test with average of coefficients
%mc = mean(coeffs);
mc = mean(coeffs(:,:));
f = 220;
Px = [mc(1) mc(end)-f];
r = roots(Px);
%r(1)
%r(2)
fprintf(1, 'we want a frequency of %f Hz\n', f);
fprintf(1, 'this root should give us a frequency of %f Hz\n', polyval(mc, r(1)));

%kTens = min(r);
kTens = mass*(min(r)^2);

%pm = [linspace(0, p(1), 1000)'; p(1)*ones(22500,1)];
pm = p(1)*ones(N,1);
fs = 44100;
y = voiceModelv02(kTens, pm, fs);

S = specgram(y, winSize);
[R,M] = extractrax(abs(S), 0.1);
F = R*fs/winSize;   % go from bins to freq
        
fprintf(1, 'detected freq: %f\n', min(min(F)));
        
tt = 0:1/fs:0.5-(1/fs);
ysin = sin(2*pi*f*tt);

% plot reed frequency as a function of sounding frequency
% from the table and the fitted model
figure
plot(w0,f0SoundMat(1,:));
hold on
plot(w0,polyval(mc,w0),'r')
xlabel('reed frequency')
ylabel('sounding frequency (Hz)');
title('fitted frequency function - f0SoundMatV2, polyfit order 2');
legend('table trajectory', 'fitted trajectory');
grid on

% save best and worst for the fitted function
tableTraj = f0SoundMat(1,:);
fittedTraj = polyval(mc, w0);
[distWorst,worstInd] = max((tableTraj-fittedTraj).^2);
[distBest,bestInd] = min((tableTraj-fittedTraj).^2);

fprintf(1, 'best frequency match: %f, dist: %f\n', f0(bestInd), distBest);
fprintf(1, 'worst frequency match: %f, dist: %f\n', f0(worstInd), distWorst);


%%%%%
% do the best frequency for 4th order fit
%%%%
f = f0(bestInd);
Px = [mc(1:2) mc(end)-f];
r = roots(Px);
fprintf(1, 'we want a frequency of %f Hz\n', f);
fprintf(1, 'this root should give us a frequency of %f Hz\n', polyval(mc, r(2)));

kTens = r(2);   % the real root

pm = [linspace(0, p(1), 1000)'; p(1)*ones(22500,1)];
fs = 44100;
y = voiceModelv02(kTens, pm, fs);

S = specgram(y, winSize);
[R,M] = extractrax(abs(S), 0.1);
F = R*fs/winSize;   % go from bins to freq
        
fprintf(1, 'detected freq: %f\n', min(min(F)));
        
tt = 0:1/fs:0.5-(1/fs);
ysin = sin(2*pi*f*tt);

%wavwrite(0.9*ysin, fs, 'tablefit/sine_f0SoundMat2_polyfit2_best.wav');
%wavwrite(y, fs, 'tablefit/voice_f0SoundMat2_polyfit2_best.wav');


%%%%%
% do the worst frequency
%%%%
f = f0(worstInd);
Px = [mc(1:2) mc(end)-f];
r = roots(Px);
fprintf(1, 'we want a frequency of %f Hz\n', f);
fprintf(1, 'this root should give us a frequency of %f Hz\n', polyval(mc, r(1)));

%%% for some reason r(2) is negative...let's just try a different one that
%%% looks bad

f = f0(worstInd);
Px = [mc(1:2) mc(end)-f];
r = roots(Px);
fprintf(1, 'we want a frequency of %f Hz\n', f);
fprintf(1, 'this root should give us a frequency of %f Hz\n', polyval(mc, r(2)));

kTens = r(2);   % the real root

pm = [linspace(0, p(1), 1000)'; p(1)*ones(22500,1)];
fs = 44100;
y = voiceModelv02(kTens, pm, fs);

S = specgram(y, winSize);
[R,M] = extractrax(abs(S), 0.1);
F = R*fs/winSize;   % go from bins to freq
        
fprintf(1, 'detected freq: %f\n', min(min(F)));
        
tt = 0:1/fs:0.5-(1/fs);
ysin = sin(2*pi*f*tt);

%wavwrite(0.9*ysin, fs, 'tablefit/sine_f0SoundMat2_polyfit2_worst.wav');
%wavwrite(y, fs, 'tablefit/voice_f0SoundMat2_polyfit2_worst.wav');

