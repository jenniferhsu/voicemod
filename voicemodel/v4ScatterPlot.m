% plot V4 f0SoundMat

v4Settings
load f0SoundMatV4.mat

Nsamps = length(p)*length(k);
pressure = zeros(Nsamps,1);
rf = zeros(Nsamps,1);
soundingFreq = zeros(length(p)*length(k),1);

cnt = 1;
for i=1:length(p)
    for j=1:length(k)
        pressure(cnt) = p(i);
        rf(cnt) = w0(j)/(2*pi);
        soundingFreq(cnt) = f0SoundMat(i,j);
        cnt = cnt + 1;
    end
end

% plot data
figure;
scatter3(pressure,rf,soundingFreq,'k')
xlabel('pressure (Pa)', 'FontSize', 11);
ylabel('valve frequency (Hz)', 'FontSize', 11);
zlabel('sounding freq (Hz)', 'FontSize', 11);
title('sounding frequency for different valve frequency and pressure pairs', 'FontSize', 11);

figure;
mesh(w0, p, f0SoundMat)
xlabel('reed frequency');
ylabel('pressure (Pa)');
zlabel('sounding freq (Hz)');