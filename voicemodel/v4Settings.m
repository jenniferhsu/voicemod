% v4Settings
% script that loads all the settings for v4

mass = 0.00028;         % voice model, reed mass 
addpath('../freqTraj/sinemodel');
winSize = 1024;         % window size for extracting f0

% tension range: from frequency range of voice
mid = 50:0.5:84;
f0 = (2.^((mid-69)./12)) * 440;
w0 = f0*2*pi;
k = mass*w0.*w0;
k(k>8000) = [];
w0 = w0(1:length(k));

% pressure range
pRange = [200, 2000];
p = pRange(1):100:pRange(2);

% length of signal and sample rate
N = 22050;
fs = 44100;



