% script for testing the accuracy of the sounding frequency of the
% synthesized voice sounds

mass = 0.00028;         % voice model, reed mass 
addpath('../freqTraj/sinemodel');
winSize = 1024;         % window size for extracting f0

% tension range: from frequency range of voice
mid = 50:0.5:84;
f0 = (2.^((mid-69)./12)) * 440;
w0 = f0*2*pi;
k = mass*w0.*w0;
k(k>8000) = [];
w0 = w0(1:length(k));
f0 = f0(1:length(k));

% pressure range
pRange = [250, 1950];
p = pRange(1):100:pRange(2);

% length of signal and sample rate
N = 22050;
fs = 44100;

f0TestMat = zeros(length(p), length(k));

for i=1:length(p)
    
	% for each pressure value 
    fprintf(1, 'synthesizing signals for pressure = %f\n', p(i));
    pm = p(i)*ones(N,1);
    
    for j=1:length(f0)

        % for each sounding frequency
        
        % synthesize a sound with chosen pressure and sounding frequency
        fVec = f0(j)*ones(N,1);
        y = voiceModelv04(fVec, pm, fs, 'secAreaData/A-bart44100.txt');
        
        % analyze for fundamental frequency of synthesis
        S = specgram(y, winSize);
        [R,M] = extractrax(abs(S), 0.1);
        F = R*fs/winSize;   % go from bins to frequency

%         if (j==1 && i==11) ...
%             || (j==6 && (i==1 || (i>=4 && i<11) || (i>=12 && i <=17))) ...
%             || (j==7 && i==2)
%             mf = min(F);
%             f = min(mf(2:end));
%             f0TestMat(i,j) = f;
%         else
            f0TestMat(i,j) = min(min(F));
%         end
        
    end
end

% FIXES
% i=10; j=1;
j = 6;
for i=[1,4,5,6,7,8,9,10,12,13,14,15,16,17,18]
pm = p(i)*ones(N,1);
fVec = f0(j)*ones(N,1);

y = voiceModelv04(fVec, pm, fs, 'secAreaData/A-bart44100.txt');

% analyze for fundamental frequency of synthesis
S = specgram(y, winSize);
[R,M] = extractrax(abs(S), 0.1);
F = R*fs/winSize;   % go from bins to frequency
mf = min(F);
f = min(mf(2:end));
f0TestMat(i,j) = f;
end
% more fixes
j = 57; i = 13;
j = 60; i = 6;
j = 57; i = 10;
j = 57; i = 12;
j = 59; i = 14;
pm = p(i)*ones(N,1);
fVec = f0(j)*ones(N,1);
y = voiceModelv04(fVec, pm, fs, 'secAreaData/A-bart44100.txt');
% analyze for fundamental frequency of synthesis
S = specgram(y, winSize);
[R,M] = extractrax(abs(S), 0.1);
F = R*fs/winSize;   % go from bins to frequency
f = max(F);
f0TestMat(i,j) = f;

i = 4; j = 52;
pm = p(i)*ones(N,1);
fVec = f0(j)*ones(N,1);
y = voiceModelv04(fVec, pm, fs, 'secAreaData/A-bart44100.txt');
% analyze for fundamental frequency of synthesis
S = specgram(y, winSize);
[R,M] = extractrax(abs(S), 0.1);
F = R*fs/winSize;   % go from bins to frequency
f = max(min(F));
f0TestMat(i,j) = f;

save('f0TestMat', 'f0TestMat');

%% compute error
f0Mat = repmat(f0, size(f0TestMat,1), 1);
% difference between true sounding freq and desired sounding freq
f0Diff = abs(f0TestMat - f0Mat);

maxDiff = max(f0Diff(:));
minDiff = min(f0Diff(:));
meanDiff = mean(f0Diff(:));
sdDiff = std(f0Diff(:));
medianDiff = median(f0Diff(:));

fprintf('\n\n*** f0TestMat results ***\n');
fprintf('the max difference in Hz is: %f\n', maxDiff);
fprintf('the min difference in Hz is: %f\n', minDiff);
fprintf('the mean of differences in Hz is: %f\n', meanDiff);
fprintf('and the standard deviation in Hz: %f\n', sdDiff);
fprintf('the median difference in Hz is: %f\n', medianDiff);
fprintf('\n\n');

% plot
imagesc(f0, p,f0Diff)
colormap(1-gray)
colorbar
xlabel('sounding frequency (Hz)', 'FontSize', 11);
ylabel('subglottal pressure (Pa)', 'FontSize', 11);
title('sounding frequency error', 'FontSize', 11);



% *** f0TestMat results ***
% the max difference in Hz is: 7.974806
% the min difference in Hz is: 0.000523
% the mean of differences in Hz is: 2.685916
% and the standard deviation in Hz: 1.634323
% the median difference in Hz is: 2.284850


