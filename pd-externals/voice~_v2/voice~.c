// ================================================================================
// name: voice~.c
// desc: extern for pd that implements an interpolating 2-port scattering junction
//      digital waveguide model for the voice with a pressure-controlled valve as
//      the excitation. this version also allows one to specify the sounding 
//      frequency of the output
//      * takes in:
//          - a subglottal pressure
//          - a desired sounding frequency
//          - a cross-sectional area list of floats
//          - a second cross-sectional area list of floats
//      * outputs
//          - the interpolated signal
// this version does not allow for morphing, but has a cross fade when you are switching
// cross sectional areas (to avoid the click)
//
// author: jennifer hsu
// date: spring 2014-winter 2015
// =================================================================================

#include "m_pd.h"
#include <stdlib.h>
#include <float.h>
#include <math.h>
#include <time.h>
#include <string.h>


#define MAX_NS 25
// physical constants
#define RHO 1.2f                  // air density, kg/m^3
#define CSOS 346.3f               // speed of sound, m/s
#define N_PRESSUREVALS 19
#define N_W0VALS 61

// variables to hold sounding frequency information
static struct
{
  t_float pVals[N_PRESSUREVALS];
  t_float w0Vals[N_W0VALS];
  t_float coeffs[N_PRESSUREVALS][2];
} sfVars = { .pVals = { 200, 300, 400, 500, 600, 700, 800, 900, 1000, 1100, 1200, 1300, 1400, 1500, 1600, 1700, 1800, 1900, 2000 },
	     .w0Vals = {922.575078, 949.608591, 977.434246, 1006.075256, 1035.555511, 1065.899604, 1097.132846, 1129.281293, 1162.371760,
			1196.431853, 1231.489982, 1267.575393, 1304.718187, 1342.949348, 1382.300768, 1422.805272, 1464.496649, 
			1507.409676, 1551.580151, 1597.04920, 1643.841908, 1692.010152, 1741.589834, 1792.622312, 1845.150155, 
			1899.217181, 1954.868493, 2012.150512, 2071.111022, 2131.799208, 2194.265693, 2258.562585, 2324.743521, 
			2392.863705, 2462.979964, 2535.150786, 2609.436374, 2685.898696, 2764.601535, 2845.610544, 2928.993297, 
			3014.819352, 3103.160302, 3194.089839, 3287.683816, 3384.020305, 3483.179669, 3585.244624, 3690.300310, 
			3798.434363, 3909.736986, 4024.301024, 4142.222045, 4263.598415, 4388.531385, 4517.125170, 4649.487041, 
			4785.727411, 4925.959928, 5070.301571, 5218.872748, },
	     .coeffs = {  
             { 0.1570778630, 12.5761422099 },
			 { 0.1576259587, 12.9000586451 },
			 { 0.1567740292, 15.9094331304 },
			 { 0.1580230129, 14.4956311913 },
			 { 0.1586937332, 14.2315753008 },
			 { 0.1586388486, 15.0465590601 },
			 { 0.1592529949, 14.5407224326 },
			 { 0.1592715306, 14.8470543414 },
			 { 0.1597922143, 14.5429735775 },
			 { 0.1597706733, 14.8028869059 },
			 { 0.1593672578, 15.9290497346 },
			 { 0.1595187635, 15.9213611059 },
			 { 0.1601610327, 15.1129227776 },
			 { 0.1612243677, 13.5623989781 },
			 { 0.1610625436, 14.0298635176 },
			 { 0.1613587422, 13.8095405971 },
			 { 0.1613651616, 14.1214757744 },
			 { 0.1615684408, 14.1772992040 },
			 { 0.1613292232, 14.7454943013 },
            }
          };



// struct type to hold all variables that have to do with the stutterstutter~ object
typedef struct _voice
{
    t_object x_obj;
    
    t_float m_fs;                     // sample rate
    
    // waveguide variables
    t_float * m_upper;                // upper rail delay line
    t_float * m_lower;                // lower rail delay line
    t_float * m_upperOld;
    t_float * m_lowerOld;
    
    // waveguide variables (2)
    t_float * m_upper2;                // upper rail delay line
    t_float * m_lower2;                // lower rail delay line
    t_float * m_upperOld2;
    t_float * m_lowerOld2;
    
    t_float m_R0;                       // left boundary reflection
    
    t_float m_BL[3];                    // right boundary filter coeffs
    t_float m_AL[3];
    t_float m_stateL[2];
    t_float m_stateOutL[2];
    
    t_float m_stateL2[2];
    t_float m_stateOutL2[2];
    
    t_float m_BRL[3];                  // transition filter coeffs
    t_float m_ARL[3];
    t_float m_stateRL[2];
    t_float m_stateOutRL[2];
    
    t_float m_BWL[5];                   // wall loss filter coeffs
    t_float m_AWL[5];
    t_float m_stateWLU[4];              // upper wall loss state
    t_float m_stateOutWLU[4];
    t_float m_stateWLL[4];              // lower wall loss state
    t_float m_stateOutWLL[4];
    
    
    t_int m_Ns, m_Ns1, m_Ns2;           // number of sections
    t_int m_Nj, m_Nj1, m_Nj2;           // number of junctions
    t_float * m_k1;                      // reflection coefficients
    t_float * m_k2;                      // reflection coefficients
    
    t_float * m_S;                      // cross sectional areas
    t_float * m_S1;
    t_float * m_S2;
    t_int m_delLineCurrent;
                                        // 1 for using S1 now
                                        // 2 for using S2 now
    t_int m_delLineSwitch;                // switch delay lines flag
    t_float m_fadeFactor;
    t_float m_fadeFactorInc;

    
    t_float m_targetAlph;               // morph factor
    t_float m_currAlph;
    
    // excitation variables
    t_float m_p2[2];                    // downstream pressure
    t_float * m_pm;                     // input pressure
    t_int m_pmCntr;
    t_int m_pmLen;
    t_float m_mass;                     // mass, kg
    t_float m_a;                        // tube radius, m
    t_float m_Z0;                       // characteristic impedance
    t_float m_aw;                       // valve width, m
    t_float m_l;                        // valve length, m
    t_float m_mu;                       // thickness of valve, m
    t_float m_x0;                       // initial displacement, m
    t_float m_Q;                        // quality factor that determines damping
    t_float m_eta;                      // normalized displacement ratio
    t_float m_nu;                       // displacement shape, exponent
    t_float m_c;                        // bilinear transform scalar
    t_float m_Unm1[2];                  // last flow value
    t_float m_dUnm1[2];                 // last flow deriv
    t_float m_stateF[2];                // force filter output state
    t_float m_xn[2];                    // force filter input state
    t_float m_stateF2[2];                // force filter output state
    t_float m_xn2[2];                    // force filter input state
    
    // ramp frequency
    t_float m_targetFreq;   // target reed frequency
    t_float m_currFreq;     // current reed frequency

} t_voice;

// voice~ class that will be created in 'setup'
// and used in 'new' to create new instances
t_class * voice_class;

/* function prototypes */
static t_int *voice_perform(t_int *w);
static void voice_dsp(t_voice *x, t_signal **sp);
static void voice_free(t_voice *x);
void voice_listS1(t_voice *x, t_symbol *selector, int argc, t_atom *argv);
void interpSFunc(t_float * Svec, t_int thisSlen, t_int otherSlen);
static void *voice_new(void);
void voice_tilde_setup(void);


// perform function (audio callback)
static t_int *voice_perform(t_int *w)
{
    
    // extract data
    t_voice *x = ( t_voice * )(w[1]);    // voice object
    t_float *in = (t_float *)(w[2]);     // pressure input
    t_float *freqIn = (t_float *)(w[3]); // frequency input
    t_float *out = (t_float *)(w[4]);    // audio output buffer
    int blockSize = (int)(w[5]);        
    
    // waveguide variables
    t_float inSamp, outSamp[2], lpOut, rlOut;
    t_float upperFirst, upperLast, lowerFirst, lowerLast;
    t_float outL, out0;
    int i;
    
    // interpolate based on alph value
    float oneOverBlockSize = 1.0f/blockSize;
    float alphInc = (x->m_targetAlph - x->m_currAlph) * oneOverBlockSize;
    //float frInc = (x->m_targetFreq - x->m_currFreq) * oneOverBlockSize;
    
    // exciation variables
    t_float pm, dp, A, fr, w0, kTens, dd, ee, dUdt;
    t_float a0, a1, a2, g;
    t_float F, Fm, Fb, FU;
    t_float U, xOut, excSamp;
    
    // if we need to switch delay lines now (told to us from list update function)
    if(x->m_delLineSwitch == 1)
    {
        // check which one we are currently outputing
        if(x->m_delLineCurrent == 1)
        {
            // set up S2? well S2 should be set up in the list update function
            // so here, we'll just modify....the incrementing?
            x->m_fadeFactor = 1.0f;
            x->m_fadeFactorInc = -1.0f/22050.0f;
            x->m_delLineCurrent = 2;
        } else if(x->m_delLineCurrent == 2)
        {
            // S1 should be set up in the list update function
            x->m_fadeFactor = 0.0f;
            x->m_fadeFactorInc = 1.0f/22050.0f;
            x->m_delLineCurrent = 1;
        }
        x->m_delLineSwitch = 0;
    }
    
    
    // process samples
    int sample, delLine;
    for(sample = 0; sample < blockSize; sample++)
    {
        
        for(delLine = 0; delLine < 2; delLine++)
        {
        
            /* GENERATE EXCITATION */

            // pressure difference
            //inSamp = *(in + sample);
            pm = *(in + sample);
            dp = fabs(x->m_p2[delLine] - pm);//x->m_pm[x->m_pmCntr]);
        
            // height, cross sectional area, flow
            if(delLine==0)
                A = x->m_aw * x->m_l * x->m_eta*(powf(fabs(x->m_xn[0]/x->m_eta),x->m_nu));
            else
                A = x->m_aw * x->m_l * x->m_eta*(powf(fabs(x->m_xn2[0]/x->m_eta),x->m_nu));
        
            dd = A/(x->m_mu*RHO);
            ee = 1.0f/(2.0f*x->m_mu*A + x->m_Unm1[delLine]/x->m_fs + FLT_MIN);
            dUdt = (x->m_dUnm1[delLine] + dd*dp - ee*x->m_Unm1[delLine]*x->m_Unm1[delLine])/(2.0f*x->m_fs);
            U = x->m_Unm1[delLine] + dUdt;
            // update last flow and derivative
            x->m_Unm1[delLine] = U;
            x->m_dUnm1[delLine] = dUdt;


            // == calculate valve frequency and kTens for desired sounding frequency ==

            // desired sounding frequency
            fr = *(freqIn + sample);

            // find pressure value closest to pm
            float minDist = FLT_MAX;
            float pval1 = sfVars.pVals[0];
            float pval2 = sfVars.pVals[1];
            float coef11 = sfVars.coeffs[0][0];
            float coef12 = sfVars.coeffs[0][1];
            float coef21 = sfVars.coeffs[0][0];
            float coef22 = sfVars.coeffs[0][1];
            for(i = 0; i < N_PRESSUREVALS; i++)
            {
                float dist = fabs(sfVars.pVals[i] - pm);
                if(dist < minDist)
                {
                    minDist = dist;
                } else if((dist > minDist) && (dist > (sfVars.pVals[1]-sfVars.pVals[0])))
                {
                    if(i >= 2)
                    {
                        pval1 = sfVars.pVals[i-2];
                        pval1 = sfVars.pVals[i-1];
                        coef11 = sfVars.coeffs[i-2][0];
                        coef12 = sfVars.coeffs[i-2][1];
                        coef21 = sfVars.coeffs[i-1][0];
                        coef22 = sfVars.coeffs[i-1][1];
                    }
                    break;
                } else if(dist >= minDist)
                {
                    if(i >= 1)
                    {
                        pval1 = sfVars.pVals[i-1];
                        pval1 = sfVars.pVals[i];
                        coef11 = sfVars.coeffs[i-1][0];
                        coef12 = sfVars.coeffs[i-1][1];
                        coef21 = sfVars.coeffs[i][0];
                        coef22 = sfVars.coeffs[i][1];
                    }
                    break;
                }
            }

            // linearly interpolate coefficients
            float li_alph = (pm - pval1)/(sfVars.pVals[1]-sfVars.pVals[0]);
            float coef1 = (1-li_alph)*coef11 + li_alph*coef21;
            float coef2 = (1-li_alph)*coef12 + li_alph*coef22;
 
            // calculate valve frequency
            w0 = (fr - coef2)/coef1;
            
            // calculate valve tension
            kTens = x->m_mass*w0*w0;
            



            // update resonant frequency coefficents according to valve tension
            g = sqrt(x->m_mass * kTens)/x->m_Q;
            a0 = x->m_mass*x->m_c*x->m_c + x->m_mass*g*x->m_c + kTens;
            a1 = -2.0f*(x->m_mass*x->m_c*x->m_c - kTens);
            a2 = x->m_mass*x->m_c*x->m_c - x->m_mass*g*x->m_c + kTens;
            
            // forcing function
            Fm = x->m_aw * x->m_l * pm;
            Fb = -(x->m_aw * x->m_l * x->m_p2[delLine]);
            if( A > 0.0f )
            {
                if( x->m_xn[0] > 0 )
                {
                    //FU = x->m_aw * x->m_mu * (x->m_pm[x->m_pmCntr] - ((RHO/2.0f)*(U/A)*(U/A)));
                    FU = x->m_aw * x->m_mu * (pm - ((RHO/2.0f)*(U/A)*(U/A)));
                } else if( x->m_xn[0] < 0 )
                {
                    //FU = -( x->m_aw * x->m_mu * ( x->m_pm[x->m_pmCntr] - ((RHO/2.0f)*(U/A)*(U/A))) );
                    FU = -( x->m_aw * x->m_mu * ( pm - ((RHO/2.0f)*(U/A)*(U/A))) );
                } else
                {
                    FU = 0.0f;
                }
            } else
            {
                FU = 0.0f;
            }
            F = Fm + Fb + FU + kTens*x->m_x0;
            
        
            // calculate displacement
            // filter
            if(delLine == 0)
            {
                xOut = (F + 2.0f*x->m_stateF[0] + x->m_stateF[1])/a0 - (a1/a0)*x->m_xn[0] - (a2/a0)*x->m_xn[1];
                x->m_stateF[1] = x->m_stateF[0];
                x->m_stateF[0] = F;
            
                // for blown-open valve configuration:
                if( xOut < 0.0f )
                    xOut = 0.0f;
            
                x->m_xn[1] = x->m_xn[0];
                x->m_xn[0] = xOut;
            } else
            {
                xOut = (F + 2.0f*x->m_stateF2[0] + x->m_stateF2[1])/a0 - (a1/a0)*x->m_xn2[0] - (a2/a0)*x->m_xn2[1];
                x->m_stateF2[1] = x->m_stateF2[0];
                x->m_stateF2[0] = F;
                
                // for blown-open valve configuration:
                if( xOut < 0.0f )
                    xOut = 0.0f;
                
                x->m_xn2[1] = x->m_xn2[0];
                x->m_xn2[0] = xOut;
            }
            

            // calculate input sample (pressure = impedance * volume flow)
            excSamp = x->m_Z0 * U;
            
            
            /* PROPAGATE THROUGH WAVEGUIDE */
            if(delLine == 0)
            {
            
                // save old upper/lower rails
                for(i = 0; i < x->m_Ns1; i++)
                {
                    *(x->m_upperOld+i) = *(x->m_upper+i);
                    *(x->m_lowerOld+i) = *(x->m_lower+i);
                }
                
                upperFirst = *(x->m_upper+0);
                lowerFirst = *(x->m_lower+0);
                upperLast = *(x->m_upper+x->m_Ns1-1);
                lowerLast = *(x->m_lower+x->m_Ns1-1);
                
                // wall losses (should divide by a0, but a0 = 1)
                outL = upperLast;
                out0 = lowerFirst;
                
                // left boundary
                *(x->m_upper+0) = out0*x->m_R0 + excSamp; // write to delay line
                
                // right boundary (should have a divide by a0, but a0=1)
                lpOut = (x->m_BL[0] * outL) + (x->m_BL[1] * x->m_stateL[0]) + (x->m_BL[2] * x->m_stateL[1])\
                - (x->m_AL[1] * x->m_stateOutL[0]) - (x->m_AL[2] * x->m_stateOutL[1]);
                
                *(x->m_lower + x->m_Ns1-1) = lpOut;  // write to delay line
                
                // update states
                x->m_stateL[1] = x->m_stateL[0];
                x->m_stateL[0] = outL;
                x->m_stateOutL[1] = x->m_stateOutL[0];
                x->m_stateOutL[0] = lpOut;
                
                // propagate through scattering junction
                for(int m = 0; m < x->m_Nj1; m++)
                {
                    *(x->m_lower+m) = *(x->m_upperOld+m)* *(x->m_k1+m) + *(x->m_lowerOld+m+1)*(1 - *(x->m_k1+m));
                    *(x->m_upper+m+1) = *(x->m_upperOld+m) * (1.0f + *(x->m_k1+m)) + *(x->m_lowerOld+m+1)*(-1.0f)* *(x->m_k1+m);
                }
                
                // update downstream pressure
                x->m_p2[0] = *(x->m_upper+0) + *(x->m_lower+0);
                
            } else
            {
                
                // save old upper/lower rails
                for(i = 0; i < x->m_Ns2; i++)
                {
                    *(x->m_upperOld2+i) = *(x->m_upper2+i);
                    *(x->m_lowerOld2+i) = *(x->m_lower2+i);
                }
                
                upperFirst = *(x->m_upper2+0);
                lowerFirst = *(x->m_lower2+0);
                upperLast = *(x->m_upper2+x->m_Ns2-1);
                lowerLast = *(x->m_lower2+x->m_Ns2-1);
                
                // wall losses (should divide by a0, but a0 = 1)
                outL = upperLast;
                out0 = lowerFirst;
                
                // left boundary
                *(x->m_upper2+0) = out0*x->m_R0 + excSamp; // write to delay line
                
                // right boundary (should have a divide by a0, but a0=1)
                lpOut = (x->m_BL[0] * outL) + (x->m_BL[1] * x->m_stateL2[0]) + (x->m_BL[2] * x->m_stateL2[1])\
                - (x->m_AL[1] * x->m_stateOutL2[0]) - (x->m_AL[2] * x->m_stateOutL2[1]);
                
                *(x->m_lower2 + x->m_Ns2-1) = lpOut;  // write to delay line
                
                // update states
                x->m_stateL2[1] = x->m_stateL2[0];
                x->m_stateL2[0] = outL;
                x->m_stateOutL2[1] = x->m_stateOutL2[0];
                x->m_stateOutL2[0] = lpOut;
                
                // propagate through scattering junction
                for(int m = 0; m < x->m_Nj2; m++)
                {
                    *(x->m_lower2+m) = *(x->m_upperOld2+m)* *(x->m_k2+m) + *(x->m_lowerOld2+m+1)*(1 - *(x->m_k2+m));
                    *(x->m_upper2+m+1) = *(x->m_upperOld2+m) * (1.0f + *(x->m_k2+m)) + *(x->m_lowerOld2+m+1)*(-1.0f)* *(x->m_k2+m);
                }
                
                // update downstream pressure
                x->m_p2[1] = *(x->m_upper2+0) + *(x->m_lower2+0);
            }
            
            
            if( outL > 1.0f )
                outL = 1.0f;
            else if( outL < -1.0f )
                outL = -1.0f;
            outSamp[delLine] = outL;
            
            
        }
        
        /* SWITCHING WAVEGUIDES (kind of so that we can get rid of clicks) */
        *(out+sample) = x->m_fadeFactor*outSamp[0] + (1.0f-x->m_fadeFactor)*outSamp[1];
        if(x->m_delLineCurrent == 1)
        {
            if(1.0 - x->m_fadeFactor <= 0.005f)
            {
                x->m_fadeFactorInc = 0.0f;
            }
        } else
        {
            if(x->m_fadeFactor - 0.0f <= 0.005f)
            {
                x->m_fadeFactorInc = 0.0f;
            }
        }
        x->m_fadeFactor += x->m_fadeFactorInc;

    }
    
    return (w+6);
}

// called when dsp starts, register audio callback
static void voice_dsp(t_voice * x, t_signal **sp)
{
    // save this here because I can't get it to work in the add function...
    x->m_fs = sp[0]->s_sr;
    // add the perform function
    dsp_add(voice_perform, 5, x, sp[0]->s_vec, sp[1]->s_vec, sp[2]->s_vec, sp[0]->s_n);
    
}

             
// list callback - update cross sectional areas
void voice_listS1(t_voice *x, t_symbol *selector, int argc, t_atom *argv)
{
    
    x->m_delLineSwitch = 1;
    
    if(x->m_delLineCurrent == 2)
    {
        // update S1
    
        // update internal pointer about list length
        x->m_Ns1 = argc;
        x->m_Nj1 = x->m_Ns1-1;
        
        int i;
        // copy list into our S1
        for(i = 0; i < argc; i++)
        {
            // error check
            if(argv[i].a_type != A_FLOAT)
            {
                post("arguments in list must be numbers");
                return;
            }
            x->m_S1[i] = argv[i].a_w.w_float;
        }
        
        // calculate reflection coefficient
        for(i = 0; i < x->m_Nj1; i++ )
        {
            x->m_k1[i] = (x->m_S1[i] - x->m_S1[i+1]) / (x->m_S1[i] + x->m_S1[i+1]);
        }
        
        // clear upper and lower
        for(i = 0; i < MAX_NS; i++)
        {
            *(x->m_upper+i) = 0.0f;
            *(x->m_lower+i) = 0.0f;
        }
        // clear variables and filter state
        x->m_p2[0] = 0.0f;
        x->m_Unm1[0] = 0.0f;                           // last flow value
        x->m_dUnm1[0] = 0.0f;                          // last flow derivative
        x->m_stateF[0] = 0.0f;                      // force filter output state
        x->m_stateF[1] = 0.0f;
        x->m_xn[0] = x->m_x0;                          // force filter input state
        x->m_xn[1] = 0.0f;
        x->m_stateL[0] = 0.0f;
        x->m_stateL[1] = 0.0f;
        x->m_stateOutL[0] = 0.0f;
        x->m_stateOutL[1] = 0.0f;
    } else
    {
        // update S2
        
        // update internal pointer about list length
        x->m_Ns2 = argc;
        x->m_Nj2 = x->m_Ns2-1;
        
        int i;
        // copy list into our S2
        for(i = 0; i < argc; i++)
        {
            // error check
            if(argv[i].a_type != A_FLOAT)
            {
                post("arguments in list must be numbers");
                return;
            }
            x->m_S2[i] = argv[i].a_w.w_float;
        }
        
        // calculate reflection coefficient
        for(i = 0; i < x->m_Nj2; i++ )
        {
            x->m_k2[i] = (x->m_S2[i] - x->m_S2[i+1]) / (x->m_S2[i] + x->m_S2[i+1]);
        }
        
        // clear upper and lower
        for(i = 0; i < MAX_NS; i++)
        {
            *(x->m_upper2+i) = 0.0f;
            *(x->m_lower2+i) = 0.0f;
        }
        // clear variables and filter state
        x->m_p2[1] = 0.0f;
        x->m_Unm1[1] = 0.0f;                           // last flow value
        x->m_dUnm1[1] = 0.0f;                          // last flow derivative
        x->m_stateF2[0] = 0.0f;                      // force filter output state
        x->m_stateF2[1] = 0.0f;
        x->m_xn2[0] = x->m_x0;                          // force filter input state
        x->m_xn2[1] = 0.0f;
        x->m_stateL2[0] = 0.0f;
        x->m_stateL2[1] = 0.0f;
        x->m_stateOutL2[0] = 0.0f;
        x->m_stateOutL2[1] = 0.0f;
    }
    
}


void interpSFunc(t_float * Svec, t_int thisSlen, t_int otherSlen)
{
    // get differences
    int i, j;
    float Sdiff[thisSlen];
    for( i = 0; i < thisSlen-1; i++ )
    {
        Sdiff[i] = fabs(Svec[i] - Svec[i+1]);
    }
    int numInterp = fabs(thisSlen - otherSlen);
    int idx[numInterp];
    int minInd = 0;
    float minVal;
    
    // need to choose the smallest difference in Sdiff
    for( i = 0; i < numInterp; i++ )
    {
        minInd = 0;
        minVal = FLT_MAX;    // just some big number that won't be in the length
        for( j = 0; j < thisSlen-1; j++ )
        {
            if(Sdiff[i] < minVal)
            {
                minInd = i;
                minVal = Sdiff[i];
            }
        }
        if( minVal == FLT_MAX )
        {
            float poop = 0.0f;
        }
        idx[i] = minInd;
        Sdiff[minInd] = FLT_MAX;
    }

    // sort the differences
    // qsort(Sdiff, sizeof(Sdiff)/sizeof(*Sdiff), sizeof(*Sdiff), comp)

    // interpolate to get new cross sectional areas in between
    float interpS[numInterp];
    for(i = 0; i < numInterp; i++)
        interpS[i] = 0.5f*(Svec[idx[i]]+Svec[idx[i]+1]);

    // insert new interpolated values in the correct place
    for(i = 0; i < numInterp; i++)
    {
        idx[i] = idx[i] + i + 1;
    }
    int totalLen;
    if( thisSlen > otherSlen )
        totalLen = thisSlen;
    else if( otherSlen > thisSlen )
        totalLen = otherSlen;
    else
        totalLen = thisSlen; //thisSlen + otherSlen;
    float newS[totalLen];
    // copy over old S array
    for( i = 0; i < thisSlen; i++ )
    {
        newS[i] = Svec[i];
    }
    // insert newly interpolated values at the correct positions
    int insertPos;
    for( i = 0; i < numInterp; i++ )
    {
        insertPos = idx[i];
        // shift over values in newS
        for( j = totalLen-1; j > insertPos; j-- )
        {
            newS[j] = newS[j-1];
        }
        // put in the new value
        newS[insertPos] = interpS[i];
    }
    
    // copy over new S to the S that we were given
    for( i = 0; i < totalLen; i++ )
    {
        Svec[i] = newS[i];
    }

    
}

// clean up memory
static void voice_free(t_voice *x)
{
    free(x->m_upper);
    free(x->m_lower);
    free(x->m_upperOld);
    free(x->m_lowerOld);
    free(x->m_upper2);
    free(x->m_lower2);
    free(x->m_upperOld2);
    free(x->m_lowerOld2);
    free(x->m_k1);
    free(x->m_k2);
    free(x->m_S1);
    free(x->m_S2);
}

// new function that is called everytime we create a new voice object in pd
static void *voice_new(void)
{
    // create object from class
    t_voice * x = (t_voice *)pd_new(voice_class);
    
    // === inlets and outlets ===
    inlet_new(&x->x_obj, &x->x_obj.ob_pd, &s_signal, &s_signal);                        // frequency inlet
    inlet_new(&x->x_obj, &x->x_obj.ob_pd, gensym("list"), gensym("S1"));                // list
    //inlet_new(&x->x_obj, &x->x_obj.ob_pd, gensym("list"), gensym("S2"));                // list

    outlet_new(&x->x_obj, gensym("signal"));                                            // output signal
    // ===========================
    
    // left reflection ceoff
    x->m_R0 = 0.7f;
    
    // right reflection
    x->m_BL[0] = -0.2162f;
    x->m_BL[1] = -0.2171f;
    x->m_BL[2] = -0.0545f;
    x->m_AL[0] = 1.0f;
    x->m_AL[1] = -0.6032f;
    x->m_AL[2] = 0.0910f;
    x->m_stateL[0] = 0.0f;
    x->m_stateL[1] = 0.0f;
    x->m_stateOutL[0] = 0.0f;
    x->m_stateOutL[1] = 0.0f;
    
    x->m_stateL2[0] = 0.0f;
    x->m_stateL2[1] = 0.0f;
    x->m_stateOutL2[0] = 0.0f;
    x->m_stateOutL2[1] = 0.0f;
    
    // transition filter coeffs
    x->m_BRL[0] = x->m_BL[0] + x->m_AL[0];
    x->m_BRL[1] = x->m_BL[1] + x->m_AL[1];
    x->m_BRL[2] = x->m_BL[2] + x->m_AL[2];
    x->m_ARL[0] = x->m_AL[0];
    x->m_ARL[1] = x->m_AL[1];
    x->m_ARL[2] = x->m_AL[2];
    x->m_stateRL[0] = 0.0f;
    x->m_stateRL[1] = 0.0f;
    x->m_stateOutRL[0] = 0.0f;
    x->m_stateOutRL[1] = 0.0f;
    
    // wall loss filters
    x->m_BWL[0] = 0.806451596106077f;
    x->m_BWL[1] = -1.855863155228909f;
    x->m_BWL[2] = 1.371191452991298f;
    x->m_BWL[3] = -0.312274852426121f;
    x->m_BWL[4] = -0.006883256612646f;
    x->m_AWL[0] = 1.0f;
    x->m_AWL[1] = -2.392436499871960f;
    x->m_AWL[2] = 1.891289981326362f;
    x->m_AWL[3] = -0.511406512428537f;
    x->m_AWL[4] = 0.015235504020645f;
    for( int i = 0; i < 4; i++ )
    {
        x->m_stateWLU[i] = 0.0f;
        x->m_stateWLL[i] = 0.0f;
        x->m_stateOutWLU[i] = 0.0f;
        x->m_stateOutWLL[i] = 0.0f;
    }
    
    // digital waveguide (2-port scattering junction) variables
    x->m_Ns = 20;
    x->m_Nj = x->m_Nj-1;
    x->m_Ns1 = 20;//1;
    x->m_Nj1 = x->m_Ns1-1;
    x->m_Nj2 = 20;//1;
    x->m_Nj2 = x->m_Nj2-1;
    x->m_S = (t_float *)malloc(sizeof(t_float) * MAX_NS);
    x->m_S1 = (t_float *)malloc(sizeof(t_float) * MAX_NS);
    x->m_S2 = (t_float *)malloc(sizeof(t_float) * MAX_NS);
    x->m_k1 = (t_float *)malloc(sizeof(t_float) * MAX_NS);
    x->m_k2 = (t_float *)malloc(sizeof(t_float) * MAX_NS);
    for(int i = 0; i < MAX_NS; i++)
    {
        x->m_S[i] = 0.0f;
        x->m_S1[i] = 0.0f;
        x->m_S2[i] = 0.0f;
        x->m_k1[i] = 0.0f;
        x->m_k2[i] = 0.0f;
    }
    for(int i = 0; i < x->m_Ns; i++)
    {
        x->m_S[i] = 1.0f;
        x->m_S1[i] = 1.0f;
        x->m_S2[i] = 1.0f;
        x->m_k1[i] = 0.0f;
        x->m_k2[i] = 0.0f;
    }
    
    //x->m_lpInPrev = 0.0f;
    
    // sample rate (should be modified in dsp function in case it is different)
    x->m_fs = 44100.0f;
    
    x->m_upper = (t_float *)malloc(sizeof(t_float) * MAX_NS);
    x->m_lower = (t_float *)malloc(sizeof(t_float) * MAX_NS);
    x->m_upperOld = (t_float *)malloc(sizeof(t_float) * MAX_NS);
    x->m_lowerOld = (t_float *)malloc(sizeof(t_float) * MAX_NS);
    
    x->m_upper2 = (t_float *)malloc(sizeof(t_float) * MAX_NS);
    x->m_lower2 = (t_float *)malloc(sizeof(t_float) * MAX_NS);
    x->m_upperOld2 = (t_float *)malloc(sizeof(t_float) * MAX_NS);
    x->m_lowerOld2 = (t_float *)malloc(sizeof(t_float) * MAX_NS);
    
    // zero out delay buffers
    long i;
    for(i = 0; i < MAX_NS; i++)
    {
        *(x->m_upper+i) = 0.0f;
        *(x->m_lower+i) = 0.0f;
        *(x->m_upper2+i) = 0.0f;
        *(x->m_lower2+i) = 0.0f;
    }
    
    x->m_targetAlph = 0.0f;
    x->m_currAlph = 0.0f;
    
    // excitation variable initialization
    x->m_p2[0] = x->m_p2[1] = 0.0f;                             // downstream pressure
    x->m_pmLen = 66150;
    x->m_pmCntr = 0;
    int Nattack = 4410;
    int Ndecay = 8820;
    x->m_pm = (t_float *)malloc(x->m_pmLen*sizeof( float ));
    // create a linear ramp
    float ma = 800.0f / ( float )(Nattack-1.0f);
    float md = -800.0f / ( float )Ndecay;
    float bd = 800.0f*( ( float )x->m_pmLen) / ( float )Ndecay;
    for( i = 0; i < x->m_pmLen; i++ )
    {
        if( i < Nattack )
        {
            x->m_pm[i] = ( float )i*ma;
        } else if( i >= Nattack && i < x->m_pmLen - Ndecay )
        {
            x->m_pm[i] = 800.0f;
        } else
        {
            x->m_pm[i] = ( float )i*md + bd;
        }
        
    }
    
    
    
    x->m_mass = 0.00028;                        // mass, kg
    x->m_a = 0.014f;                            // tube radius, m
    x->m_Z0 = (RHO*CSOS)/(M_PI*x->m_a*x->m_a);  // characteristic impedance
    x->m_aw = 0.0049f;                          // valve width, m
    x->m_l = 0.014f;                            // valve length, m
    x->m_mu = 0.003f;                           // thickness of valve, m
    x->m_x0 = .0002f;                           // initial displacement, m
    x->m_Q = 9.0f;                              // quality factor that determines damping
    x->m_eta = 0.0007f;                         // normalized displacement ratio
    x->m_nu = 0.5f;                             // displacement shape, exponent
    x->m_c = 2.0f*x->m_fs;                      // bilinear transform scalar
    x->m_Unm1[0] = x->m_Unm1[1] = 0.0f;                           // last flow value
    x->m_dUnm1[0] = x->m_dUnm1[1] = 0.0f;                          // last flow derivative
    
    x->m_stateF[0] = 0.0f;                      // force filter output state
    x->m_stateF[1] = 0.0f;
    x->m_xn[0] = x->m_x0;                       // force filter input state
    x->m_xn[1] = 0.0f;
    
    x->m_stateF2[0] = 0.0f;                      // force filter output state
    x->m_stateF2[1] = 0.0f;
    x->m_xn2[0] = x->m_x0;                       // force filter input state
    x->m_xn2[1] = 0.0f;

    
    // switching between delay lines
    x->m_delLineSwitch = 0;
    x->m_fadeFactor = 1.0f;
    x->m_fadeFactorInc = 0.0f;
    x->m_delLineCurrent = 1;


    // debug print sfVars
    /*
    post("subglottal pressures");
    for(int i = 0; i < 19; i++)
      {
	post("%f", sfVars.pVals[i]);
      }
    post("valve frequencies");
    for(int i = 0; i < 61; i++)
      {
	post("%f", sfVars.w0Vals[i]);
      }
    post("coefficients");
    for(int i = 0; i < 19; i++)
      {
	post("%f %f", sfVars.coeffs[i][0], sfVars.coeffs[i][1]);
      }
    */


    return (x);
}

// called when Pd is first loaded and tells Pd how to load the class
void voice_tilde_setup(void)
{
    voice_class = class_new(gensym("voice~"), (t_newmethod)voice_new, (t_method)voice_free, sizeof(t_voice), 0, 0);
    
    // magic to declare the leftmost inlet the main inlet that will take a signal
    // installs delayTime as the leftmost inlet float
    CLASS_MAINSIGNALIN(voice_class, t_voice, m_targetAlph);
    
    // method that will be called when dsp is turned on
    class_addmethod(voice_class, (t_method)voice_dsp, gensym("dsp"), (t_atomtype)0);
    
    // register callback methods for right inlets
    class_addmethod(voice_class, (t_method)voice_listS1, gensym("S1"), A_GIMME, 0);
    //class_addmethod(voice_class, (t_method)voice_listS2, gensym("S2"), A_GIMME, 0);
    
    
    
}

