#include "m_pd.h"
#include <math.h>
#ifdef NT
#pragma warning( disable : 4244 )
#pragma warning( disable : 4305 )
#endif

/* ------------------------ freqTracksv1~ ----------------------------- */
/*
 Input: FFT real and imaginary parts
 */

/* tilde object to take absolute value. */

static t_class *freqTracksv1_class;

typedef struct _freqTracksv1
{
    t_object x_obj; 
    t_float x_f;    	/* place to hold inlet's value if it's set by message */
} t_freqTracksv1;

    /* this is the actual performance routine which acts on the samples.
    It's called with a single pointer "w" which is our location in the
    DSP call list.  We return a new "w" which will point to the next item
    after us.  Meanwhile, w[0] is just a pointer to dsp-perform itself
    (no use to us), w[1] and w[2] are the input and output vector locations,
    and w[3] is the number of points to calculate. */
static t_int *freqTracksv1_perform(t_int *w)
{
    t_float *inReal = (t_float *)(w[1]);
    t_float *inImag = (t_float *)(w[2]);
    t_float *out = (t_float *)(w[3]);
    int bufferSamps = (int)(w[4]);

    int sample;
    float f, fr, fi;
    for(sample = 0; sample < bufferSamps; sample++)
    {
      fr = *(inReal+sample);
      fi = *(inImag+sample);
      // calculate the magnitude?
      f = sqrt(powf(fr, 2) + powf(fi, 2));


      *(out+sample) = f;
    }
    return (w+5);
}

    /* called to start DSP.  Here we call Pd back to add our perform
    routine to a linear callback list which Pd in turn calls to grind
    out the samples. */
static void freqTracksv1_dsp(t_freqTracksv1 *x, t_signal **sp)
{
  dsp_add(freqTracksv1_perform, 4, sp[0]->s_vec, sp[1]->s_vec, sp[2]->s_vec, sp[0]->s_n);
}

static void *freqTracksv1_new(void)
{
    t_freqTracksv1 *x = (t_freqTracksv1 *)pd_new(freqTracksv1_class);
    inlet_new(&x->x_obj, &x->x_obj.ob_pd, &s_signal, &s_signal);
    outlet_new(&x->x_obj, gensym("signal"));
    x->x_f = 0;

    return (x);
}

    /* this routine, which must have exactly this name (with the "~" replaced
    by "_tilde) is called when the code is first loaded, and tells Pd how
    to build the "class". */
void freqTracksv1_tilde_setup(void)
{
    freqTracksv1_class = class_new(gensym("freqTracksv1~"), (t_newmethod)freqTracksv1_new, 0,
    	sizeof(t_freqTracksv1), 0, A_DEFFLOAT, 0);
	    /* this is magic to declare that the leftmost, "main" inlet
	    takes signals; other signal inlets are done differently... */
    CLASS_MAINSIGNALIN(freqTracksv1_class, t_freqTracksv1, x_f);
    	/* here we tell Pd about the "dsp" method, which is called back
	when DSP is turned on. */
    class_addmethod(freqTracksv1_class, (t_method)freqTracksv1_dsp, gensym("dsp"), 0);
}
