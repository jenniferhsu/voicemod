voicemod

author: Jennifer Hsu (jsh008@ucsd.edu)
date: winter 2015

This is a physical voice model implementation.  The vocal tract is implemented as a digital waveguide.  The vocal folds are implemented as a simplified blown-open reed.  The goals in this project include:

* finding a way to specify the sounding frequency of voice model
* extracting a gesture from a saxophone sound and using that gesture to control the voice model
* morphing between different vowel sounds in the voice model
